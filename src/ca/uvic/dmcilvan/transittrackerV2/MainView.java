package ca.uvic.dmcilvan.transittrackerV2;

import java.io.PrintWriter;
import java.io.StringWriter;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import ca.uvic.dmcilvan.transittrackerV2.R;
import ca.uvic.dmcilvan.transittrackerV2.control.ActiveTripController;
import ca.uvic.dmcilvan.transittrackerV2.database.DatabaseManager;
import ca.uvic.dmcilvan.transittrackerV2.utilities.Test;
import ca.uvic.dmcilvan.transittrackerV2.utilities.Utilities;
import ca.uvic.dmcilvan.transittrackerV2.view.TransitMap;
import ca.uvic.dmcilvan.transittrackerV2.view.TripSearchDialogFragment;


/**
 * Main view for the app, handles all user input and GPS.
 * 
 * @author Daniel
 * 
 */
public class MainView extends FragmentActivity implements LocationListener {

	// Time to wait for a better location before reducing requirements (5
	// seconds).
	private static final int TIME_INCREMENT = 1000 * 5 * 1;
	// Minimum accuracy for GPS in metres.
	private static final int MIN_ACCURACY = 120;
	// Accuracy increments for location in metres
	private static final int ACCURACY_INCREMENT = 10;
	// Default viewing area in metres
	private static final int DEFAULT_VIEWING_AREA = 4000;

	private static final boolean RUN_TESTS = false;

	private volatile boolean appReady = false;
	private boolean uncaughtExceptionThrown = false;
	private String stackTrace = null;
	private TransitMap map;
	private static ActiveTripController activeTripController;

	private volatile TextView statusTextView;
	private static volatile String statusTextString = null;
	private volatile TextView routeTextView;
	private static volatile String routeTextString = null;
	private volatile TextView tripTextView;
	private static volatile String tripTextString = null;
	private volatile Button realTimeButton = null;

	private LocationManager locationManager;
	private static Location lastLocation = null;
	private static long gpsTimeOffset = 0;
	private static long networkTimeOffset = 0;
	private double longitudeScale = 1D;
	private double averageLon = 0D;

	private static MainView context = null;
	private volatile boolean runMemoryMonitor = true;
	private boolean focused = true;
	private static boolean runRealTime = false;

	// private final int COORDMULT = Integer.MAX_VALUE / 1000;

	// private String routes[];

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	/**
	 * Sets the context for the rest of the app to access.
	 * 
	 * @param newContext
	 *            The current MainView
	 */
	private static void setContext(MainView newContext) {
		context = newContext;
	}

	/**
	 * Gets the context (MainView) of the app.
	 * 
	 * @return Context for the app.
	 */
	public static MainView getContext() {
		return context;
	}

	/**
	 * Initializes the app, starting to listen for GPS as well as setting up the
	 * text views and restoring any text they might have been set on a previous
	 * run. Also sets the buttons to invisible until the app is ready to run.
	 */
	private void init() {
		appReady = false;
		try {
			Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
				public void uncaughtException(Thread t, Throwable e) {
					StringWriter sw = new StringWriter();
					PrintWriter pw = new PrintWriter(sw);
					e.printStackTrace(pw);
					stackTrace = sw.toString();
					uncaughtExceptionThrown = true;
					updateAppStatus(stackTrace);
					e.printStackTrace();
				}
			});
		} catch (SecurityException e) {
			System.out.println("Exception handler did not start.");
		}

		System.out.println("Inint!");
		setContext(this);
		runMemoryMonitor = true;
		startMemoryMonitor();

		setContentView(R.layout.activity_main_view);

		findViewById(R.id.RouteDown).setVisibility(View.INVISIBLE);
		findViewById(R.id.RouteUp).setVisibility(View.INVISIBLE);
		findViewById(R.id.TripDown).setVisibility(View.INVISIBLE);
		findViewById(R.id.TripUp).setVisibility(View.INVISIBLE);
		findViewById(R.id.findTripButton).setVisibility(View.INVISIBLE);
		findViewById(R.id.toggleRealTimeButton).setVisibility(View.INVISIBLE);

		statusTextView = (TextView) findViewById(R.id.status);
		routeTextView = (TextView) findViewById(R.id.RouteName);
		tripTextView = (TextView) findViewById(R.id.TripName);
		realTimeButton = (Button) findViewById(R.id.toggleRealTimeButton);
		if (runRealTime) {
			realTimeButton.setText(getString(R.string.toggle_realtime_on));
		} else {
			realTimeButton.setText(getString(R.string.toggle_realtime_off));
		}

		map = (TransitMap) findViewById(R.id.mapView);

		if (statusTextString != null) {
			statusTextView.setText(statusTextString);
		}
		if (routeTextView != null) {
			routeTextView.setText(routeTextString);
		}
		if (tripTextView != null) {
			tripTextView.setText(tripTextString);
		}

		locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
			locationManager.requestLocationUpdates(
					LocationManager.NETWORK_PROVIDER, TIME_INCREMENT, 0, this);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
				TIME_INCREMENT, 0, this);

		prepareDatabase();
	}

	/**
	 * Periodically checks the memory usage of the app.
	 */
	private void startMemoryMonitor() {
		new Thread(new Runnable() {
			public void run() {
				while (runMemoryMonitor) {
					try {
						System.out.println("MEMORY:"
								+ (Utilities.getCurrentMem() / 1048576F));
						System.out.println("MEMORY: MAX ALLOWABLE "
								+ (Utilities.getMaxMem() / 1048576F));
						Thread.sleep(20000);
					} catch (InterruptedException e) {
					}
				}
				System.out.println("MEMORY: Shutting down Memory Monitor!");
			}
		}).start();
	}

	/**
	 * Initializes the database manager, also creates a new database if there is
	 * not one setup.
	 * 
	 * Calls finishLoading() once the database is ready.
	 */
	public synchronized void prepareDatabase() {
		new Thread(new Runnable() {
			public void run() {
				DatabaseManager.getInstance();
				finishLoading();
			}
		}).start();
	}

	/**
	 * Initializes the map and starts the man processing parts of the app once
	 * the database is ready. Sets buttons to be visible.
	 * 
	 * Sets the current location to the last know value.
	 */
	private void finishLoading() {
		System.out.println("finishLoading");
		if (RUN_TESTS) {
			new Test().run(0, 10, 0);
			finish();
		} else {

			if (activeTripController == null) {
				activeTripController = ActiveTripController.getInstance();
				activeTripController.startThreads();
			}
			activeTripController.clearObservers();
			activeTripController.addObserver(map);

			// Start the map
			double lat;
			double lon;

			averageLon = Double.parseDouble(DatabaseManager.getInstance()
					.getConfig("averageLon"));
			longitudeScale = Double.parseDouble(DatabaseManager.getInstance()
					.getConfig("longitudeScale"));

			if (lastLocation == null) {
				lat = 48.464570;
				lon = -123.311169;
			} else {
				lat = lastLocation.getLatitude();
				lon = lastLocation.getLongitude();
			}

			map.initialize(DEFAULT_VIEWING_AREA, lat, averageLon
					- (averageLon - lon) * longitudeScale);
			map.activateMap(true);
			activeTripController.setLocation(lat, averageLon
					- (averageLon - lon) * longitudeScale);

			this.runOnUiThread(new Runnable() {
				public void run() {
					findViewById(R.id.RouteDown).setVisibility(View.VISIBLE);
					findViewById(R.id.RouteUp).setVisibility(View.VISIBLE);
					findViewById(R.id.TripDown).setVisibility(View.VISIBLE);
					findViewById(R.id.TripUp).setVisibility(View.VISIBLE);
					findViewById(R.id.findTripButton).setVisibility(
							View.VISIBLE);
					findViewById(R.id.toggleRealTimeButton).setVisibility(
							View.VISIBLE);
				}
			});
			// loc1 = locationManager
			// .getLastKnownLocation(LocationManager.GPS_PROVIDER);
			// if (loc1 != null) {
			// onLocationChanged(loc1);
			// activeTripController.setLocation(loc1.getLatitude(), averageLon
			// - (averageLon - loc1.getLongitude()) * longitudeScale);
			// }

			appReady = true;

			// Trip t1 = DatabaseManager.getInstance().getTripById("2579773");
			// Trip t2 = DatabaseManager.getInstance().getTripById("2579773");
			// System.out.println("\t\t\t\t************** Frechet Distance 2 willows -> 2 willows:"
			// + new FrechetDistance(t1, t2).getDist());
			// t1 = DatabaseManager.getInstance().getTripById("2579773");
			// t2 = DatabaseManager.getInstance().getTripById("2581071");
			// System.out.println("\t\t\t\t************** Frechet Distance 2 willows -> 11 tillicum:"
			// + new FrechetDistance(t1, t2).getDist());
			// t1 = DatabaseManager.getInstance().getTripById("2580469");
			// t2 = DatabaseManager.getInstance().getTripById("2581071");
			// System.out.println("\t\t\t\t************** Frechet Distance early 2 willows -> 11 tillicum:"
			// + new FrechetDistance(t1, t2).getDist());

			// Location loc1;
			// Location loc2;
			// loc1 = locationManager
			// .getLastKnownLocation(LocationManager.GPS_PROVIDER);
			// if (locationManager
			// .isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
			// loc2 = locationManager
			// .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			// } else {
			// loc2 = loc1;
			// }
			// if (loc1 != null && loc2 != null) {
			// System.out.println("Old location set");
			// if (loc2.getTime() < loc1.getTime()) {
			// onLocationChanged(loc2);
			// onLocationChanged(loc1);
			// } else {
			// onLocationChanged(loc1);
			// onLocationChanged(loc2);
			// }
			// }

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main_view, menu);
		return true;
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		if (activeTripController != null)
			activeTripController.stopThreads();
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		focused = false;
		locationManager.removeUpdates(this);
		runMemoryMonitor = false;
		// if (activeTripController != null)
		// activeTripController.pauseThreads();
		super.onPause();
	}

	@Override
	protected void onResume() {
		focused = true;
		if (activeTripController != null)
			activeTripController.resumeThreads();
		super.onResume();
		init();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putString("trip", tripTextString);
		outState.putString("route", routeTextString);
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		tripTextString = savedInstanceState.getString("trip");
		routeTextString = savedInstanceState.getString("route");
		super.onRestoreInstanceState(savedInstanceState);
	}

	/**
	 * Writes a message to the status display at the bottom of the app.
	 * 
	 * @param newStatus
	 *            Message to display.
	 */
	public void updateAppStatus(final String newStatus) {
		System.out.println(newStatus);
		this.runOnUiThread(new Runnable() {
			public void run() {
				if (!uncaughtExceptionThrown) {
					statusTextString = newStatus;
					statusTextString += "\n"
							+ ((int) (Utilities.getCurrentMem() / 104857.6F) / 10F)
							+ "MB";
					statusTextView.setText(statusTextString);
				} else {
					statusTextView.setText("Oops...\n\n" + stackTrace);
				}
			}
		});
	}

	public void onLocationChanged(Location location) {
		if (!appReady)
			return;

		if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
			// Maintain the GPS time offset.
			if (gpsTimeOffset == 0) {
				gpsTimeOffset = System.currentTimeMillis() - location.getTime();
			}
			gpsTimeOffset = (long) ((0.9) * gpsTimeOffset + (0.1) * (System
					.currentTimeMillis() - location.getTime()));

		} else {
			// Maintain the network time offset.
			if (networkTimeOffset == 0) {
				networkTimeOffset = System.currentTimeMillis()
						- location.getTime();
			}
			networkTimeOffset = (long) ((0.9) * networkTimeOffset + (0.1) * (System
					.currentTimeMillis() - location.getTime()));
		}

		if (lastLocation == null)
			lastLocation = location;
		if (locationQualityCheck(location)) {
			double convertedLongitude = averageLon
					- (averageLon - location.getLongitude()) * longitudeScale;
			activeTripController.setLocation(location.getLatitude(),
					convertedLongitude);
			lastLocation = location;
		}
		// updateAppStatus(statusText + "( i=" + (locationLocks++) +")");
	}

	/**
	 * Determines if a new location is useful based on its accuracy and the time
	 * since the last fix.
	 * 
	 * @param location
	 *            The Location passed from the GPS or Network providers.
	 * @return Should this location be used.
	 */
	private boolean locationQualityCheck(Location location) {

		long deltaT;
		if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
			deltaT = location.getTime() - gpsTimeOffset
					- lastLocation.getTime();
		} else {
			deltaT = location.getTime() - networkTimeOffset
					- lastLocation.getTime();
		}
		float deltaAcc = location.getAccuracy() - lastLocation.getAccuracy();

		// If the accuracy is still good, grab a new one.
		if (deltaT > 0 && deltaAcc <= 0) {
			return true;
		}

		// If its been too long reduce requirements.
		for (int i = 0; i * ACCURACY_INCREMENT < MIN_ACCURACY; i++) {
			if (deltaT > i * TIME_INCREMENT
					&& location.getAccuracy() < i * ACCURACY_INCREMENT) {
				return true;
			}
		}

		return false;
	}

	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	public void routeUp(View view) {
		activeTripController.nextRoute();
		activeTripController.findBestTripsForCurrentRoute();
	}

	public void routeDown(View view) {
		activeTripController.prevRoute();
		activeTripController.findBestTripsForCurrentRoute();
	}

	public void tripUp(View view) {
		activeTripController.nextTrip();
	}

	public void tripDown(View view) {
		activeTripController.prevTrip();
	}

	public void findTrip(View view) {
		activeTripController.findBestTripsForAllRoutes();
	}

	public void setTripText(final String trip) {
		this.runOnUiThread(new Runnable() {
			public void run() {
				tripTextString = trip;
				tripTextView.setText(tripTextString);
			}
		});
	}

	public void setRouteText(final String route) {
		this.runOnUiThread(new Runnable() {
			public void run() {
				routeTextString = route;
				routeTextView.setText(route);
			}
		});
	}

	public void showTripSelectionDialogue() {
		if (focused) {
			FragmentManager framgentManager = getSupportFragmentManager();
			TripSearchDialogFragment dialogue = new TripSearchDialogFragment();
			dialogue.show(framgentManager, "trip selection dialogue");
		}
	}

	public void onFinishInputDialog(int selectedTrip) {
		ActiveTripController.getInstance().onFinishInputDialog(selectedTrip);
		System.out.println("MainView close dialgogue");
	}

	public void toggleRealTime(View view) {
		runRealTime = !runRealTime;
		if (runRealTime) {
			realTimeButton.setText(getString(R.string.toggle_realtime_on));
			ActiveTripController.getInstance().startRealTime();
		} else {
			realTimeButton.setText(getString(R.string.toggle_realtime_off));
			ActiveTripController.getInstance().stopRealTime();
		}
	}
}
