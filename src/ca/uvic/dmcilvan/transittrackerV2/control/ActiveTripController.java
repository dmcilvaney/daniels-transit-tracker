package ca.uvic.dmcilvan.transittrackerV2.control;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import ca.uvic.dmcilvan.transittrackerV2.MainView;
import ca.uvic.dmcilvan.transittrackerV2.control.TripSelector.TripSelectorCaller;
import ca.uvic.dmcilvan.transittrackerV2.database.DatabaseManager;
import ca.uvic.dmcilvan.transittrackerV2.entity.ActiveTrip;
import ca.uvic.dmcilvan.transittrackerV2.entity.PathSegment;
import ca.uvic.dmcilvan.transittrackerV2.entity.Route;
import ca.uvic.dmcilvan.transittrackerV2.entity.Stop;
import ca.uvic.dmcilvan.transittrackerV2.entity.TimedStop;
import ca.uvic.dmcilvan.transittrackerV2.entity.Trip;
import ca.uvic.dmcilvan.transittrackerV2.utilities.Utilities;

/**
 * @author Daniel
 * 
 */
public class ActiveTripController implements TripSelectorCaller {

	// protected static final int MAXTHREADS = 50;
	// protected static final int MAXMEM = 14 * 1048576;

	private static ActiveTripController instance;
	private List<ActiveTripControllerObserver> observers;
	private int timeOfLastNotify = 0;
	private Lock activteTripLock;
	private TripSelector selector;
	private volatile boolean runEstimator = true;

	private int lat;
	private int lon;
	private List<Trip> trips;
	private List<Route> routes;
	private Route currentRoute;
	private int currentRouteNum = -1;
	private Trip currentTrip;
	private int currentTripNum;
	private List<TimedStop> currentTimedStops;
	private ActiveTrip activeTrip;

	private PathSegment closest = null;
	private PathSegment estimatedLocation = null;
	private String[] nearTripStrings = null;
	private List<Trip> nearTrips = null;

	private RealTimeTripSearch realTimeTripSearch;
	private boolean runRealTime = false;

	private BusController busController;

	private ActiveTripController() {
		observers = new ArrayList<ActiveTripControllerObserver>();
		activteTripLock = new ReentrantLock();
		routes = DatabaseManager.getInstance().getAllRoutes();
	}

	/**
	 * Periodically updates the theoretical location of the bus on the current
	 * trip.
	 */
	private void runTheoreticalLocationCalculator() {
		new Thread(new Runnable() {
			public void run() {
				while (runEstimator) {
					try {
						calculateTheoreticalLocation();
						int sleepTime = 0;
						if (estimatedLocation == null) {
							sleepTime = 7000;
						} else {
							sleepTime = estimatedLocation.timeSpent;
						}
						if (sleepTime >= 7000) {
							sleepTime = 7000;
						}
						Thread.sleep(sleepTime);
					} catch (InterruptedException e) {
					}
				}
			}
		}).start();
	}

	private void runRealTimeTripSearch() {
		System.out.println("Starting real time search");
		if (realTimeTripSearch != null) {
			realTimeTripSearch.requestStop();
		}
		realTimeTripSearch = new RealTimeTripSearch(this);
		new Thread(realTimeTripSearch).start();
	}

	/**
	 * Asks the active thread for the current theoretical location of the bus on
	 * the trip.
	 */
	private void calculateTheoreticalLocation() {
		PathSegment estimate = null;

		activteTripLock.lock();
		if (currentTrip != null && activeTrip != null) {
			estimate = activeTrip.getTheoreticalPathSegment();
		}
		activteTripLock.unlock();
		if (estimatedLocation != estimate) {
			estimatedLocation = estimate;
		}
		if (Utilities.getCurrentTime() - timeOfLastNotify > 5000) {
			notifyObservers();
		}
	}

	/**
	 * Gets the theoretical location of the bus on the active trip.
	 * 
	 * @return Path segment on the trip where the bus should be.
	 */
	public PathSegment getCalculatedLocation() {
		return estimatedLocation;
	}

	/**
	 * Gets an instance of ActiveTripController
	 * 
	 * @return The running instance of ActiveTripController
	 */
	public static ActiveTripController getInstance() {
		if (instance == null)
			instance = new ActiveTripController();
		return instance;
	}

	/**
	 * Updates the activeTrip with new route/trip selections. Resets all
	 * calculated values for the old active trip.
	 */
	private void update() {
		stopTripSearch();
		activteTripLock.lock();
		closest = null;
		activeTrip = null;
		estimatedLocation = null;
		currentTimedStops = null;

		if (currentTrip != null) {
			currentTimedStops = DatabaseManager.getInstance()
					.getTimedStopsByTrip(currentTrip.getId());

		}
		if (currentRoute != null && currentTrip != null
				&& currentTimedStops != null) {
			activeTrip = new ActiveTrip(currentRoute, currentTrip,
					currentTimedStops);
			updateLocationInfo();

		}
		activteTripLock.unlock();
		calculateTheoreticalLocation();
		notifyObservers();
	}

	/**
	 * Sets the current location of the GPS. Converts from absolute lat, lon to
	 * offset integer: (lat+360) * COORDMULT.
	 * 
	 * @param lat
	 *            Latitude in absolute decimal form
	 * @param lon
	 *            Longitude in absolute decimal form
	 */
	public void setLocation(double lat, double lon) {
		this.lat = (int) (Utilities.COORDMULT * (lat + 360));
		this.lon = (int) (Utilities.COORDMULT * (lon + 360));
		updateLocationInfo();
		notifyObservers();
	}

	/**
	 * Updates timing information about the current trip
	 */
	private void updateLocationInfo() {
		activteTripLock.lock();
		if (activeTrip != null && MainView.getContext() != null) {

			int calculatedTimeSpent = activeTrip.getTrip().getStartTime();
			for (PathSegment p : activeTrip.getPath()) {
				calculatedTimeSpent += p.timeSpent;
			}

			closest = activeTrip.getClostestPathSegment(this.lat, this.lon);
			MainView.getContext()
					.updateAppStatus(
							"Estimated Arrival: "
									+ Utilities.convertTimeMSecToS(Utilities
											.getCurrentTime()
											+ estimateArrival())
									+ " Calc: "
									+ Utilities
											.convertTimeMSecToS(calculatedTimeSpent)
									+ "\nDelta: "
									+ Utilities.convertTimeMSecToS(calculatedTimeSpent
											- (Utilities.getCurrentTime() + estimateArrival())));
		}
		activteTripLock.unlock();
	}

	/**
	 * Gets the current route.
	 * 
	 * @return The current route.
	 */
	public Route getRoute() {
		return currentRoute;
	}

	/**
	 * Calculates an estimated arrival time for a bus at the closest location.
	 * 
	 * @return time in milliseconds until the bus reaches the end of the trip.
	 */
	private int estimateArrival() {
		int timeEstimate = 0;
		activteTripLock.lock();
		List<PathSegment> path = activeTrip.getPath();
		for (int i = activeTrip.getClosestIndex(lat, lon); i < path.size(); i++) {
			timeEstimate += path.get(i).timeSpent;
		}
		activteTripLock.unlock();

		return timeEstimate;
	}

	/**
	 * Notifes all observers that the active trip has been updated.
	 */
	private synchronized void notifyObservers() {
		timeOfLastNotify = Utilities.getCurrentTime();
		for (ActiveTripControllerObserver o : observers) {
			o.notify(this);
		}
	}

	/**
	 * Adds an observer which should be updated when the active route changes.
	 * 
	 * @param newObserver
	 *            A new observer which should be notified
	 */
	public void addObserver(ActiveTripControllerObserver newObserver) {
		observers.add(newObserver);
	}

	/**
	 * Returns the closest path segment to the current location for the active
	 * trip.
	 * 
	 * @return
	 */
	public PathSegment getClosest() {
		return closest;
	}

	/**
	 * Sets the current route to a specific route.
	 * 
	 * @param routeNum
	 *            The route number to select, sorted by name.
	 */
	public void setRoute(int routeNum) {
		if (routes.size() > routeNum) {
			currentRoute = routes.get(routeNum);
			currentRouteNum = routeNum;
			trips = DatabaseManager.getInstance().getTripsByRoute(
					currentRoute.getId());
			MainView.getContext().setRouteText(currentRoute.getName());
		} else {
			currentRoute = null;
			currentRouteNum = -1;
			MainView.getContext().setRouteText(
					"There are not " + (routeNum + 1)
							+ " trips stored for today.");
		}
		// setTrip() handles calling update();
		setTrip(0);
	}

	/**
	 * Sets the current route to the next route, sorted by name.
	 */
	public void nextRoute() {
		if (routes.size() > 0) {
			currentRouteNum = (currentRouteNum + 1) % routes.size();
			currentRoute = routes.get(currentRouteNum);

			trips = DatabaseManager.getInstance().getTripsByRoute(
					currentRoute.getId());
			MainView.getContext().setRouteText(currentRoute.getName());
		} else {
			currentRoute = null;
			MainView.getContext().setRouteText("No routes found");
		}
		// setTrip() handles calling update();
		setTrip(0);
	}

	/**
	 * Sets the current route to the previous route, sorted by name.
	 */
	public void prevRoute() {
		if (routes.size() > 0) {
			currentRouteNum = (currentRouteNum - 1) % routes.size();
			if (currentRouteNum < 0)
				currentRouteNum += routes.size();
			currentRoute = routes.get(currentRouteNum);
			trips = DatabaseManager.getInstance().getTripsByRoute(
					currentRoute.getId());
			MainView.getContext().setRouteText(currentRoute.getName());
		} else {
			currentRoute = null;
			MainView.getContext().setRouteText("No routes found");
		}
		// setTrip() handles calling update();
		setTrip(0);
	}

	/**
	 * Sets the current trip to a specific trip for the current route.
	 * 
	 * @param tripNum
	 *            The trip number to select, sorted by start time.
	 */
	public void setTrip(int tripNum) {
		if (currentRoute == null || trips == null) {
			MainView.getContext().setTripText("No route selected");
		} else if (trips.size() > tripNum) {
			currentTrip = trips.get(tripNum);
			currentTripNum = tripNum;
			update();
			MainView.getContext().setTripText(currentTrip.getName());
		} else {
			currentTrip = null;
			currentTripNum = -1;
			update();
			MainView.getContext().setTripText(
					"There are not " + (tripNum + 1) + " routes stored.");
		}
	}

	/**
	 * Set a trip based on a trip object.
	 * 
	 * @param newTrip
	 *            The new trip to select.
	 */
	private void setTrip(Trip newTrip) {
		if (newTrip != currentTrip) {
			int i = 0;
			for (Route r : DatabaseManager.getInstance().getAllRoutes()) {
				if (r.getId().equals(newTrip.getRouteId())) {
					setRoute(i);
					break;
				}
				i++;
			}
			i = 0;
			for (Trip t : trips) {
				if (t.getId().equals(newTrip.getId())) {
					setTrip(i);
					break;
				}
				i++;
			}
		}
	}

	/**
	 * Sets the active trip to the next trip for the current route, sorted by
	 * start time.
	 */
	public void nextTrip() {
		if (currentRoute == null || trips == null) {
			MainView.getContext().setTripText("No route selected");
			return;
		}
		if (trips.size() > 0) {
			currentTripNum = (currentTripNum + 1) % trips.size();
			currentTrip = trips.get(currentTripNum);
			System.out.println("New trip: " + currentTrip.getId());
			update();
			MainView.getContext().setTripText(currentTrip.getName());
		} else {
			update();
			MainView.getContext().setTripText("No trip found");
		}
	}

	/**
	 * Sets the active trip to the previous trip for the current route, sorted
	 * by start time.
	 */
	public void prevTrip() {
		if (currentRoute == null || trips == null) {
			MainView.getContext().setTripText("No route selected");
			return;
		}

		if (trips.size() > 0) {
			currentTripNum = (currentTripNum - 1) % trips.size();
			if (currentTripNum < 0)
				currentTripNum += trips.size();
			currentTrip = trips.get(currentTripNum);
			System.out.println("New trip: " + currentTrip.getId());
			// System.out.println("New Trip:" + currentTripNum + "("
			// + currentTrip.getStartTime());
			update();
			MainView.getContext().setTripText(currentTrip.getName());
		} else {
			update();
			MainView.getContext().setTripText("No trip found");
		}
	}

	/**
	 * Returns the active trip's calculated path.
	 * 
	 * @return The active trip's calculated path, null if no active trip.
	 */
	public List<PathSegment> getPath() {
		if (activeTrip != null)
			return activeTrip.getPath();
		else
			return null;
	}

	/**
	 * Current latitude as set by setLocation().
	 * 
	 * @return latitude converted to int.
	 */
	public int getLon() {
		return lon;
	}

	/**
	 * Current longitude as set by setLocation().
	 * 
	 * @return longitude converted to int.
	 */
	public int getLat() {
		return lat;
	}

	/**
	 * Returns the stops of the current trip
	 * 
	 * @return Current trip's stops, or null if no trip
	 */
	public List<Stop> getCurrentStops() {
		if (activeTrip != null)
			return activeTrip.getStops();
		else
			return null;
	}

	/**
	 * Starts all worker threads (Currently just the location estimator)
	 */
	public void startThreads() {
		activteTripLock.lock();
		System.out.println("Starting threads");
		runEstimator = true;
		runTheoreticalLocationCalculator();
		if (busController == null) {
			busController = new BusController();
			new Thread(busController).start();
		}
		if (runRealTime) {
			runRealTimeTripSearch();
		}
		activteTripLock.unlock();
	}

	/**
	 * Stops all worker threads
	 */
	public void stopThreads() {
		activteTripLock.lock();
		runEstimator = false;
		if (busController != null) {
			busController.requestStop();
		}
		if (realTimeTripSearch != null) {
			realTimeTripSearch.requestStop();
		}
		activteTripLock.unlock();
	}

	public void startRealTime() {
		runRealTime = true;
		runRealTimeTripSearch();
	}

	public void stopRealTime() {
		runRealTime = false;
		if (realTimeTripSearch != null) {
			realTimeTripSearch.requestStop();
			realTimeTripSearch = null;
		}
	}

	public void pauseThreads() {
		runEstimator = false;
		if (busController != null) {
			busController.requestStop();
			busController = null;
		}
	}

	public void resumeThreads() {
		runEstimator = true;
		runTheoreticalLocationCalculator();
		if (busController == null) {
			busController = new BusController();
			new Thread(busController).start();
		}
		// if (busController != null) {
		// busController.requestResume();
		// }
	}

	/**
	 * Selects the best guess for a trip for the CURRENT route only.
	 */
	public void findBestTripsForCurrentRoute() {
		System.out.println("Find for current route");
		LinkedList<Route> route = new LinkedList<Route>();
		route.add(currentRoute);
		findBestTripsForRoutes(route);
	}

	/**
	 * Selects the best guess for a trip from any of the routes given.
	 * 
	 * @param routes
	 *            Routes to check for trips.
	 */
	public void findBestTripsForRoutes(Collection<Route> routes) {
		stopTripSearch();
		selector = new TripSelector(lat, lon, this);
		selector.findTrip(routes, 1500);
	}

	/**
	 * Selects the best guess for a trip from all near by routes, routes are
	 * selected using the routeSearch() method of the DatabaseManager.
	 */
	public void findBestTripsForAllRoutes() {
		stopTripSearch();
		selector = new TripSelector(lat, lon, this);
		selector.findTrip(getNearByRoutes(), 0);
	}

	public Collection<Route> getNearByRoutes() {
		int searchDist = 8;
		Set<Route> routeSet = new HashSet<Route>();
		for (int i = -searchDist; i <= searchDist; i++) {
			for (int j = -searchDist; j <= searchDist; j++) {
				for (Route r : DatabaseManager.getInstance().routeSearch(
						(this.lat / Utilities.ROUNDING) * Utilities.ROUNDING
								+ i * Utilities.ROUNDING,
						(this.lon / Utilities.ROUNDING) * Utilities.ROUNDING
								+ j * Utilities.ROUNDING)) {
					routeSet.add(r);
				}
			}
		}
		System.out.println("Done Near selection");
		return routeSet;
	}

	/**
	 * If there is a currently running trip selector this method requests that
	 * it stop.
	 */
	private void stopTripSearch() {
		if (selector != null) {
			selector.stop();
		}
	}

	/*
	 * (non-Javadoc) *
	 * 
	 * @see com.uvic.dmcilvan.transittrackerV2.control.RouteSelectorCaller#
	 * handleSortedTrips(java.util.List)
	 */
	public void handleSortedTrips(List<Trip> sortedTrips) {
		int h = 1;
		for (Trip t : sortedTrips) {
			System.out.print((h++)
					+ ": ("
					+ t.routeSearchScore
					+ ") "
					+ DatabaseManager.getInstance()
							.getRouteById(t.getRouteId()).getName() + ": ");
			System.out.println(t.getName());
		}

		if (sortedTrips.size() > 0) {
			nearTripStrings = new String[sortedTrips.size()];
			nearTrips = sortedTrips;
			int i = 0;
			for (Trip t : sortedTrips) {
				String listValue = "";
				listValue += " -- "
						+ DatabaseManager.getInstance()
								.getRouteById(t.getRouteId()).getShortName();
				listValue += ": " + t.getName();
				nearTripStrings[i++] = listValue;
			}
			MainView.getContext().showTripSelectionDialogue();
		}
		// pickTrip(sortedTrips);
	}

	/**
	 * From a list of possible trips, picks the one with the lowest cost from
	 * the list and sets it to be the active trip.
	 * 
	 * @param nearTrips
	 */
	private void pickTripFromNearTrips(int tripSelection) {
		// TODO maybe add locks here?
		if (nearTrips.size() > 0) {
			Trip bestTrip = nearTrips.get(tripSelection);
			setTrip(bestTrip);
		}
	}

	/**
	 * Locks the active trip mutex so any external classes can iterate through
	 * returned lists in a thread safe manner.
	 */
	public void lock() {
		activteTripLock.lock();
		if (busController != null) {
			busController.lock();
		}
	}

	/**
	 * Unlocks active trip mutex.
	 */
	public void unlock() {
		if (busController != null) {
			busController.unlock();
		}
		activteTripLock.unlock();
	}

	public List<TimedStop> getTimedStops() {
		return currentTimedStops;
	}

	public void onFinishInputDialog(int selectedTrip) {
		System.out.println("Handling dialogue input!");
		pickTripFromNearTrips(selectedTrip);
	}

	public String[] getTripSearchList() {
		if (nearTripStrings == null)
			nearTripStrings = new String[0];
		return nearTripStrings;
	}

	public void newRealTimeTrip(Trip bestTrip) {
		setTrip(bestTrip);
	}

	public List<Bus> getActiveBuses() {
		if (busController != null) {
			return busController.getBuses();
		} else {
			return new LinkedList<Bus>();
		}
	}

	public List<PathSegment> getHistoricPath() {
		if (realTimeTripSearch == null) {
			return null;
		} else {
			return realTimeTripSearch.getHistoricPath();
		}

	}

	public Trip getTrip() {
		return currentTrip;
	}

	public void clearObservers() {
		observers.clear();
	}

}
