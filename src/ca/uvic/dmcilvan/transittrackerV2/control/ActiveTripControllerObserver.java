package ca.uvic.dmcilvan.transittrackerV2.control;

public interface ActiveTripControllerObserver {
    public void notify(ActiveTripController activeTripController);
}
