package ca.uvic.dmcilvan.transittrackerV2.control;

import java.util.List;

import ca.uvic.dmcilvan.transittrackerV2.database.DatabaseManager;
import ca.uvic.dmcilvan.transittrackerV2.entity.PathSegment;
import ca.uvic.dmcilvan.transittrackerV2.entity.Route;
import ca.uvic.dmcilvan.transittrackerV2.entity.Trip;
import ca.uvic.dmcilvan.transittrackerV2.utilities.Utilities;


public class Bus implements Runnable {
    public final String name;
    public PathSegment currentLocation;
    public PathSegment lastLocation;
    private List<PathSegment> path;
    private int nextIndex = 0;
    private boolean run = true;
    private Thread thisThread;
    private boolean pause = false;

    public Bus(Trip t) {
	Route route = DatabaseManager.getInstance()
		.getRouteById(t.getRouteId());
	if (route == null || t.getPath().size() < 1) {
	    run = false;
	    name = "NO NAME";
	} else {
	    name = route.getShortName();
	    path = t.getPath();
	    currentLocation = path.get(0);
	    lastLocation = currentLocation;
	}
    }

    public void run() {
	thisThread = Thread.currentThread();
	Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
	if (!run || path.size() < 2) {
	    run = false;
	    return;
	}
	while (run) {
	    long currentTime = Utilities.getCurrentTime();
	    long goalTime = path.get(nextIndex).arrivalTime;
	    do {
		long sleepTime = goalTime - currentTime;
		try {

		    if (sleepTime > 0) {
			// System.out.println("Updating bus, sleep for "
			// + sleepTime + ", index: " + nextIndex);
			Thread.sleep(sleepTime);
		    }
		} catch (InterruptedException e) {
		}
		currentTime = Utilities.getCurrentTime();
	    } while (currentTime < goalTime && nextIndex < path.size());

	    currentTime = Utilities.getCurrentTime();
	    if (currentTime >= goalTime) {
		lastLocation = currentLocation;
		currentLocation = path.get(nextIndex++);

		// Wait for 1 minute after a trip ends.
		if (nextIndex >= path.size()) {
		    try {
			Thread.sleep(60000);
			run = false;
		    } catch (InterruptedException e) {
		    }
		}
	    }

	    while (pause) {
		try {
		    Thread.sleep(1000000);
		} catch (InterruptedException e) {
		}
	    }
	}
    }

    public PathSegment getCurrentLocation() {
	return currentLocation;
    }

    public PathSegment getLastLocation() {
	return lastLocation;
    }

    public boolean running() {
	return run;
    }

    public void requestStop() {
	run = false;
	if (thisThread != null)
	    thisThread.interrupt();
    }

    public void requestPause() {
	System.out.println("A bus has been paused");
	pause = true;
    }

    public void requestResume() {
	pause = false;
	if (thisThread != null)
	    thisThread.interrupt();
	System.out.println("A bus has been resumed");
    }

}
