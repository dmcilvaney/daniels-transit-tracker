package ca.uvic.dmcilvan.transittrackerV2.control;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import ca.uvic.dmcilvan.transittrackerV2.database.DatabaseManager;
import ca.uvic.dmcilvan.transittrackerV2.entity.Trip;
import ca.uvic.dmcilvan.transittrackerV2.utilities.Utilities;

public class BusController implements Runnable {
	private volatile boolean run = true;
	private volatile boolean pause = false;
	private List<Bus> activeBuses;
	private long lastTime = Utilities.getCurrentTime();;
	private long currentTime = Utilities.getCurrentTime();;
	private Lock lock = new ReentrantLock();
	private Thread thisThread;

	public BusController() {
		activeBuses = new LinkedList<Bus>();
		System.out.println("Creating BusController");
	}

	public void run() {
		thisThread = Thread.currentThread();
		Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
		for (Trip t : DatabaseManager.getInstance().getTripsByTime(
				Utilities.getCurrentTime(), Utilities.getCurrentTime())) {
			// System.out.println("Adding bus: (" + t.getRouteId() + ")"
			// + t.getName());
			Bus newBus = new Bus(t);
			lock.lock();
			activeBuses.add(newBus);
			lock.unlock();
			new Thread(newBus).start();
		}

		Thread.currentThread().setPriority(Thread.NORM_PRIORITY);

		while (run) {
			// System.out.println("Iterate bus controller");
			lock.lock();
			Iterator<Bus> busItr = activeBuses.iterator();
			while (busItr.hasNext()) {
				if (!busItr.next().running()) {
					busItr.remove();
				}
			}
			currentTime = Utilities.getCurrentTime();
			for (Trip t : DatabaseManager.getInstance().getTripsByStartTime(
					lastTime, currentTime)) {

				Bus newBus = new Bus(t);
				activeBuses.add(newBus);
				new Thread(newBus).start();
			}

			lastTime = currentTime;
			lock.unlock();

			if (!pause) {
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
				}
			} else {
				lock();
				for (Bus b : activeBuses) {
					b.requestPause();
				}
				unlock();
				while (pause) {
					System.out.println("Pausing Bus Controller");
					try {
						Thread.sleep(1000000);
					} catch (InterruptedException e) {
					}
				}
				lock();
				for (Bus b : activeBuses) {
					b.requestResume();
				}
				unlock();
			}

		}
		//
		// for (Bus b : activeBuses) {
		// b.requestStop();
		// }
	}

	public void lock() {
		lock.lock();
	}

	public void unlock() {
		lock.unlock();
	}

	public List<Bus> getBuses() {
		return activeBuses;
	}

	public void requestStop() {
		System.out.println("Stopping bus controller");
		run = false;
		pause = false;
		if (thisThread != null)
			thisThread.interrupt();
	}

	public void requestPause() {
		System.out.println("Pausing bus");
		System.out.println("Pausing bus done");
		pause = true;
	}

	public void requestResume() {
		pause = false;
		if (thisThread != null)
			thisThread.interrupt();
	}

}
