package ca.uvic.dmcilvan.transittrackerV2.control;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import ca.uvic.dmcilvan.transittrackerV2.control.TripSelector.TripSelectorCaller;
import ca.uvic.dmcilvan.transittrackerV2.database.DatabaseManager;
import ca.uvic.dmcilvan.transittrackerV2.entity.PathSegment;
import ca.uvic.dmcilvan.transittrackerV2.entity.Trip;
import ca.uvic.dmcilvan.transittrackerV2.entity.TripContainer;
import ca.uvic.dmcilvan.transittrackerV2.utilities.Utilities;

public class RealTimeTripSearch implements Runnable, TripSelectorCaller {
	private final static int MAX_TRIPS_TO_RECORD = 50;
	private final static int NUMBER_OF_TRIPS_TO_CHECK = 5;
	private final static float INCREASE_ALPHA = 0.15F; // Weighted running
	// average
	private final static float DECREASE_ALPHA = 0.05F;
	private final static float DECAY = 0.99F; // Decay of all scores when
	// updated.

	private boolean run = true;
	private boolean paused = false;

	// Run the search every 10 seconds
	private int sleepDelay = 5000;

	private List<PathSegment> historicPath;
	// private List<PathSegment> interpolatedHistoricPath;
	private int lastLat;
	private int lastLon;

	private boolean listReady = false;
	private List<Trip> sortedTrips = null;
	private ActiveTripController caller;
	private static Thread thisThread = null;
	private HashMap<String, TripContainer> recordedTrips;
	private TripSelector selector;
	private int timeOut;

	public RealTimeTripSearch(ActiveTripController caller) {
		this.caller = caller;
		historicPath = new ArrayList<PathSegment>(50);
		// interpolatedHistoricPath = new ArrayList<PathSegment>();
		recordedTrips = new HashMap<String, TripContainer>(
				MAX_TRIPS_TO_RECORD * 2 + 1);
	}

	public void run() {
		thisThread = Thread.currentThread();
		while (run) {
			System.out.println("Real time check starting!");
			listReady = false;
			addToHistoricPath();
			selector = new TripSelector(historicPath, lastLat, lastLon, this);
			// selector = new TripSelector(DatabaseManager.getInstance()
			// .getTripById("2612283").getPath(), lastLat, lastLon, this);
			selector.findTrip(caller.getNearByRoutes(), 0);
			// selector.findTrip(DatabaseManager.getInstance().getAllRoutes());
			while (!listReady) {
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
				}
			}
			if (!run) {
				break;
			}
			// Take the top 5 scoring trips, or fewer if there are not that
			// many. Full score is 100. Highest scoring trip is the most likely
			// trip. Adjust score based on how close the trips are.
			int numTrips = Math.min(NUMBER_OF_TRIPS_TO_CHECK,
					sortedTrips.size());
			if (numTrips > 0) {
				decay();
				if (numTrips == 1) {
					addScore(sortedTrips.get(0), 100);
				} else {
					float maxScore = sortedTrips.get(numTrips - 1).routeSearchScore;
					for (int i = 0; i < numTrips; i++) {
						Trip t = sortedTrips.get(i);
						addScore(t, (int) (100 - t.routeSearchScore / maxScore
								* 90));
					}
				}
				Trip bestTrip = null;
				float bestScore = 0;
				for (TripContainer tc : recordedTrips.values()) {
					if (tc.score > bestScore) {
						bestScore = tc.score;
						bestTrip = tc.trip;
					}
				}
				caller.newRealTimeTrip(bestTrip);
			}
			try {
				Thread.sleep(sleepDelay);
			} catch (InterruptedException e) {
			}
		}
	}

	private void addToHistoricPath() {
		System.out.println("Adding historic info: " + caller.getLat() + ", "
				+ caller.getLon());
		int newLat = caller.getLat();
		int newLon = caller.getLon();
		int time = Utilities.getCurrentTime();
		if (!(newLat == lastLat) || !(newLon == lastLon) || time > timeOut) {
			System.out.println("New historic info, adding it!");
			timeOut = time + 2 * 60 * 1000;
			historicPath.add(new PathSegment(newLat, newLon, -1, -1, null,
					null, Utilities.getCurrentTime()));
			lastLat = newLat;
			lastLon = newLon;
		}
	}

	private void decay() {
		System.out.println("Recorded Values:--------");
		Iterator<TripContainer> itr = recordedTrips.values().iterator();
		while (itr.hasNext()) {
			TripContainer tc = itr.next();
			tc.score = tc.score * (DECAY);
			System.out.println("    Trip " + tc.trip.getName()
					+ " and existing score " + tc.score);
			if (tc.score < 1) {
				itr.remove();
			}
		}
	}

	/**
	 * Adds or updates a score for a trip. If there are too many trips it
	 * removes the one with the lowest score.
	 * 
	 * @param trip
	 *            The trip to update.
	 * @param score
	 *            The latest score for it.
	 */
	private void addScore(Trip trip, int score) {
		while (recordedTrips.size() >= MAX_TRIPS_TO_RECORD) {
			TripContainer lowestScoring = null;
			float lowestScore = Integer.MAX_VALUE;
			for (TripContainer t : recordedTrips.values()) {
				if (t.score < lowestScore) {
					lowestScoring = t;
					lowestScore = t.score;
				}
			}
			recordedTrips.remove(lowestScoring.trip.getId());
		}

		TripContainer existingTripContainer = recordedTrips.get(trip.getId());
		if (existingTripContainer == null) {
			// new trips should start with a low score so they don't immediately
			// overtake older trips.
			recordedTrips.put(trip.getId(), new TripContainer(trip, 4));
		} else {
			float newScore;
			if (score > existingTripContainer.score) {
				newScore = existingTripContainer.score * (1 - INCREASE_ALPHA)
						+ score * (INCREASE_ALPHA);
			} else {
				newScore = existingTripContainer.score * (1 - INCREASE_ALPHA)
						+ score * (DECREASE_ALPHA);
			}

			existingTripContainer.score = newScore;
			System.out.println("Trip " + trip.getName()
					+ " is added with new score " + score
					+ " and existing score " + existingTripContainer.score);
		}
	}

	public void handleSortedTrips(List<Trip> sortedTrips) {
		this.sortedTrips = sortedTrips;
		int i = 0;
		for (Trip t : sortedTrips) {
			System.out.println((i++) + " " + t.getRouteId() + " " + t.getName()
					+ " " + t.routeSearchScore);
		}
		listReady = true;
		if (thisThread != null)
			thisThread.interrupt();
	}

	public void requestStop() {
		run = false;
		if (thisThread != null)
			thisThread.interrupt();
	}

	public void requestPause() {

	}

	public void requestResume() {
	}

	public List<PathSegment> getHistoricPath() {
		// return FrechetDistance.generateFrechetCurve(historicPath);
		return historicPath;
	}
}
