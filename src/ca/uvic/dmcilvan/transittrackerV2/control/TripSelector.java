package ca.uvic.dmcilvan.transittrackerV2.control;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import ca.uvic.dmcilvan.transittrackerV2.MainView;
import ca.uvic.dmcilvan.transittrackerV2.database.DatabaseManager;
import ca.uvic.dmcilvan.transittrackerV2.entity.ActiveTrip;
import ca.uvic.dmcilvan.transittrackerV2.entity.PathSegment;
import ca.uvic.dmcilvan.transittrackerV2.entity.Route;
import ca.uvic.dmcilvan.transittrackerV2.entity.Trip;
import ca.uvic.dmcilvan.transittrackerV2.utilities.FrechetDistance;
import ca.uvic.dmcilvan.transittrackerV2.utilities.Utilities;

public class TripSelector {

	private volatile boolean continueFindingTrip = true;
	private TripSelectorCaller caller;
	private volatile int completedTrips;
	private int lat;
	private int lon;
	boolean useHistoricPath = true;
	List<PathSegment> historicPath;

	private Lock findTripLock;

	public interface TripSelectorCaller {
		public void handleSortedTrips(List<Trip> sortedTrips);
	}

	/**
	 * Creates a new route selector for the current location.
	 * 
	 * @param lat
	 *            Decimal degree location
	 * @param lon
	 *            Decimal degree location
	 * @param caller
	 *            For use in callback when the threads are done.
	 */
	public TripSelector(int lat, int lon, TripSelectorCaller caller) {
		this.lat = lat;
		this.lon = lon;
		this.caller = caller;
		findTripLock = new ReentrantLock();
		useHistoricPath = false;
	}

	public TripSelector(List<PathSegment> historicPath, int lat, int lon,
			TripSelectorCaller caller) {
		this.lat = -1;
		this.lon = -1;
		System.out.println("Size of path to search: " + historicPath.size());
		// If we don't have enough historic data just use normal version.
		if (historicPath.size() < 1) {
			this.lat = lat;
			this.lon = lon;
			useHistoricPath = false;
		}
		this.caller = caller;
		System.out.println("Build Historic Path");
		this.historicPath = FrechetDistance.generateFrechetCurve(historicPath);
		findTripLock = new ReentrantLock();
	}

	/**
	 * From a list of routes given, and a location, find which trip the device
	 * is most likely on. Automatically starts one thread per processor with one
	 * extra.
	 * 
	 * @param routesToSearch
	 *            All the routes to search through
	 * @param delay
	 *            The delay before starting calculations, useful for interfaces
	 */
	public synchronized void findTrip(Collection<Route> routesToSearch,
			int delay) {
		findTrip(routesToSearch,
				Runtime.getRuntime().availableProcessors() + 1, delay);
		// findTrip(routesToSearch, 1, delay);
	}

	/**
	 * From a list of routes given, and a location, find which trip the device
	 * is most likely on.
	 * 
	 * @param routesToSearch
	 *            All the routes to search through
	 * @param numThreads
	 *            The number of threads to use to do calculations. Work is split
	 *            evenly.
	 * @param delay
	 *            The delay before starting calculations, useful for interfaces
	 */
	public synchronized void findTrip(Collection<Route> routesToSearch,
			final int numThreads, final int delay) {
		continueFindingTrip = true;
		completedTrips = 0;
		final List<Trip> tripsToCheck = new ArrayList<Trip>();
		final List<Trip> nearTrips = Collections
				.synchronizedList(new ArrayList<Trip>());
		final MainView context = MainView.getContext();

		for (Route r : routesToSearch) {
			tripsToCheck.addAll(DatabaseManager.getInstance().getTripsByRoute(
					r.getId()));
		}
		final int tripsToCalc = tripsToCheck.size();

		new Thread(new Runnable() {
			public void run() {
				// Optionally sleep for a moment so it is possible to quickly
				// move between routes in the interface without triggering a
				// search.
				if (delay > 0) {
					try {
						Thread.sleep(delay);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
				}
				long maxMemUsed = 0;
				final int currentTime = Utilities.getCurrentTime();

				// Calendars used for getting elapsed time
				Calendar c1 = GregorianCalendar.getInstance();
				Calendar c2;

				final Thread controlThread = Thread.currentThread();
				for (int i = 0; i < numThreads && continueFindingTrip; i++) {

					final int threadNum = i;
					new Thread(new Runnable() {
						public void run() {
							for (int j = threadNum; j < tripsToCalc; j += numThreads) {
								if (!continueFindingTrip) {
									controlThread.interrupt();
									return;
								}
								Trip trip = tripsToCheck.get(j);
								// Check if the trip is in a reasonable time
								// range, ignore if not.
								int timeFudgeFactor = 20 * 60 * 1000;
								if (trip.getStartTime() - timeFudgeFactor < currentTime
										&& trip.getEndTime() + timeFudgeFactor > currentTime) {

									ActiveTrip activeTrip = new ActiveTrip(trip);
									nearTrips.add(trip);
									// Check the estimated travel time.
									if (!useHistoricPath) {
										trip.routeSearchScore = calculateTripScore(activeTrip);
									} else {
										// System.out
										// .println("Calc using Frechet");
										trip.routeSearchScore = calculateFrechet(activeTrip);
									}
								}
								findTripLock.lock();
								completedTrips++;
								findTripLock.unlock();
							}
							controlThread.interrupt();
						}
					}).start();
				}
				while (continueFindingTrip) {
					findTripLock.lock();
					if (tripsToCalc <= completedTrips) {
						findTripLock.unlock();
						break;
					} else {
						findTripLock.unlock();
						long mem = Utilities.getCurrentMem();
						if (mem > maxMemUsed) {
							maxMemUsed = mem;
						}
						if (!useHistoricPath) {
							context.updateAppStatus("Checking trips ("
									+ completedTrips + "/" + tripsToCalc
									+ ") ["
									+ (completedTrips * 100 / tripsToCalc)
									+ "%]");
						} else {
							context.updateAppStatus("Checking trips using"
									+ " Frechet (" + completedTrips + "/"
									+ tripsToCalc + ") ["
									+ (completedTrips * 100 / tripsToCalc)
									+ "%]");
						}
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
						}
					}
				}

				if (continueFindingTrip) {
					synchronized (nearTrips) {
						Collections.sort(nearTrips);
						if (useHistoricPath)
							context.updateAppStatus("Checking trips with Frechet (DONE)");
						else
							context.updateAppStatus("Checking trips with (DONE)");
					}
				}

				c2 = GregorianCalendar.getInstance();
				int deltaT = (int) (c2.getTimeInMillis() - c1.getTimeInMillis());
				System.out.println("######## GET TRIP: " + numThreads
						+ " threads took " + deltaT + " to compute using "
						+ maxMemUsed / 1048576L + " MB of memory");

				findTripLock.lock();
				if (continueFindingTrip)
					System.out
							.println("****************************Done picking trips");
				caller.handleSortedTrips(nearTrips);
				findTripLock.unlock();
			}
		}).start();
	}

	/**
	 * Calculate the cost of a trip based so the best trip can be picked (ie
	 * lowest cost)
	 * 
	 * @param activeTrip
	 *            The trip to calculate the cost of.
	 * @return The cost of the trip, lower is a better choice
	 */
	private int calculateTripScore(ActiveTrip activeTrip) {
		final int DIST_TO_PATH_WEIGHT = 2;
		final int DIST_ALONG_PATH_WEIGHT = 1;
		final int DIST_DUE_TO_TIME_WEIGHT = 1;
		PathSegment theoreticalSegment = activeTrip.getTheoreticalPathSegment();
		PathSegment closestSegment = activeTrip
				.getClostestPathSegment(lat, lon);

		if (theoreticalSegment == null || closestSegment == null) {
			return Integer.MAX_VALUE;
		}

		int deltaTime = Math.abs(Utilities.getCurrentTime()
				- theoreticalSegment.arrivalTime);
		int distToPath = (int) Math.sqrt(Math.pow(lat
				- (closestSegment.lat + Utilities.ROUNDING / 2), 2)
				+ Math.pow(lon - (closestSegment.lon + Utilities.ROUNDING / 2),
						2));
		// approximate the distance along a path using the number of grid square
		// along the path.
		int indexDifference = activeTrip.getTheoreticalIndex()
				- activeTrip.getClosestIndex(lat, lon);
		int distanceAlongPath = Math.abs(indexDifference) * Utilities.ROUNDING;
		// busses are more likely to be late than early, adjust for that here.
		if (indexDifference < 0) {
			distanceAlongPath *= 2;
		}
		// Approximation of the distance away a bus is.
		int distanceTraveled = distToPath * DIST_TO_PATH_WEIGHT
				+ distanceAlongPath * DIST_ALONG_PATH_WEIGHT;

		// Convert time spent at a stop to decimal degrees traveled at
		// AVERAGE_SPEED.
		long distanceOffset = Utilities.timeToDist(deltaTime);
		int cost = (int) (distanceTraveled + distanceOffset
				* DIST_DUE_TO_TIME_WEIGHT);
		// It is possible for a cost to overflow, set to max int if this
		// happens.
		if (cost < 0) {
			cost = Integer.MAX_VALUE;
		}

		return cost;
	}

	/**
	 * Calculate the cost of a trip based on the Frechet distance so the best
	 * trip can be picked (ie lowest cost)
	 * 
	 * @param activeTrip
	 *            The trip to calculate the cost of.
	 * @return The cost of the trip, lower is a better choice
	 */
	private int calculateFrechet(ActiveTrip activeTrip) {
		int singleSegmentPenalty = 100;
		PathSegment firstHistoric = historicPath.get(0);
		PathSegment lastHistoric = historicPath.get(historicPath.size() - 1);

		int startIndex = activeTrip.getClosestIndex(firstHistoric.lat,
				firstHistoric.lon);
		int endIndex = activeTrip.getClosestIndex(lastHistoric.lat,
				lastHistoric.lon);

		// Swap the indexes around if they are backwards.
		if (startIndex > endIndex) {
			int temp = startIndex;
			startIndex = endIndex;
			endIndex = temp;
		}
		// System.out.println("Creating theoretical curve");
		List<PathSegment> theoreticalPath = activeTrip.getPath().subList(
				startIndex, endIndex + 1);
		int distance = new FrechetDistance(historicPath, theoreticalPath,
				false, true).getDist();
		// System.out.println("Distance to " + activeTrip.getTrip().getName()
		// + " is " + distance);
		if (endIndex == startIndex) {
			distance *= singleSegmentPenalty;
		}
		return distance;
	}

	/**
	 * Request that the route selector stop processing and NOT notify its
	 * creator of completion.
	 */
	public void stop() {
		findTripLock.lock();
		continueFindingTrip = false;
		findTripLock.unlock();
	}
}
