package ca.uvic.dmcilvan.transittrackerV2.database;

import java.util.Iterator;
import java.util.LinkedHashMap;

import ca.uvic.dmcilvan.transittrackerV2.entity.Trip;
import ca.uvic.dmcilvan.transittrackerV2.utilities.Utilities;

public class Cache<K, V> extends LinkedHashMap<K, V> {
	private static final long serialVersionUID = -705853748393955335L;
	private int maxSize;
	private boolean runCacheMonitor = true;
	private long maxMem = Utilities.getMaxMem();
	private Thread monitor;

	public Cache(int maxSize) {
		super(maxSize * 2, 0.5F, true);
		this.maxSize = maxSize;
		startCacheMonitor();
	}

	@Override
	public V put(K id, V trip) {
		if (this.size() >= maxSize) {
			deleteLRU();
		}
		return super.put(id, trip);
	}

	@Override
	public V get(Object key) {
		return super.get(key);
	}

	public synchronized void deleteLRU() {
		while (Utilities.getCurrentMem() >= maxMem && this.size() >= 0
				|| this.size() >= maxSize * 0.49) {
			Iterator<Entry<K, V>> itr = this.entrySet().iterator();
			for (int i = 0; i < 40 && itr.hasNext(); i++) {
				// only remove 40 entries for increased speed.
				// System.out.println("Removing " + i + " element!");
				itr.next();
				itr.remove();
			}
			System.gc();
		}
	}

	public void startMonitor() {
		runCacheMonitor = true;
		startCacheMonitor();
	}

	public void stopMonitor() {
		runCacheMonitor = false;
	}

	/**
	 * Periodically checks the memory usage of the cache and clears the least
	 * recently used as needed.
	 */
	private void startCacheMonitor() {
		if (monitor == null || !monitor.isAlive()) {
			monitor = new Thread(new Runnable() {
				public void run() {
					long lastSize = 0;
					int sleepLength = 1000;
					while (runCacheMonitor) {
						try {
							long newMemUsage = Utilities.getCurrentMem();
							if (newMemUsage - 10000 > lastSize
									|| newMemUsage >= maxMem) {
								// System.out.println("Aggresive cache monitoring"
								// + newMemUsage);
								sleepLength = 30;
							} else {
								// System.out.println("Passive cache");
								sleepLength = 300;
							}
							lastSize = newMemUsage;
							if (newMemUsage >= maxMem) {
								deleteLRU();
							}
							Thread.sleep(sleepLength);
						} catch (InterruptedException e) {
						}
					}
					System.out.println("MEMORY: Shutting down Cache Monitor!");
				}
			});
			monitor.start();
		}
	}
}
