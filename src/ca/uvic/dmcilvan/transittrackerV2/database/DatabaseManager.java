package ca.uvic.dmcilvan.transittrackerV2.database;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import ca.uvic.dmcilvan.transittrackerV2.MainView;
import ca.uvic.dmcilvan.transittrackerV2.entity.Route;
import ca.uvic.dmcilvan.transittrackerV2.entity.Stop;
import ca.uvic.dmcilvan.transittrackerV2.entity.TimedStop;
import ca.uvic.dmcilvan.transittrackerV2.entity.Trip;
import ca.uvic.dmcilvan.transittrackerV2.entity.WayPoint;
import ca.uvic.dmcilvan.transittrackerV2.utilities.Utilities;

public class DatabaseManager {
	private boolean WRITE_TO_DOWNLOADS = false;
	private static final String DBNAME = "transitTracker.db";
	protected static final int HASHMAP_SIZE = 8999;
	private static DatabaseManager instance = null;
	private static MySQLiteHelper helper = null;
	private static SQLiteDatabase db;

	private LinkedHashMap<String, Trip> tripCache;
	private Lock cacheLock;
	private boolean enableCache = true;
	private HashMap<String, Stop> stopCache;

	/**
	 * Creates a new database manager, but does not initialize it directly.
	 */
	private DatabaseManager() {
		try {
			if (WRITE_TO_DOWNLOADS) {
				MainView.getContext().deleteDatabase(DBNAME);
			}
			helper = new MySQLiteHelper(MainView.getContext(), DBNAME,
					WRITE_TO_DOWNLOADS);
			db = helper.getWritableDatabase();
		} catch (SQLiteException e) {
			e.printStackTrace();
			System.out.println("SQL Error");
		}
	}

	/**
	 * Returns the DatabaseManager instance. If the instance is not initialized
	 * it does so first. Thread safe.
	 * 
	 * @param context
	 *            MainView for the app
	 * @return
	 */
	public synchronized static DatabaseManager getInstance() {
		if (instance == null) {
			instance = new DatabaseManager();
			instance.initialze();
		}
		return instance;
	}

	/**
	 * Populates database tables if they are not already populated. This can
	 * take A LONG TIME! (one hour+). Also sets up caches for stops and trips.
	 */
	private void initialze() {
		System.out.println("Initialize database");
		// Check if the database is ready to use
		if (!helper.isDatabaseReady() && !WRITE_TO_DOWNLOADS) {
			System.out.println("Database is NOT ready");
			ConnectivityManager connManager = (ConnectivityManager) MainView
					.getContext()
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo wifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			if (wifi.isConnected()) {
				// Download the file from the internet instead of generating it
				// on the phone.
				helper.downloadDatabase("http://webhome.csc.uvic.ca/~dmcilvan/TransitTracker/"
						+ DBNAME);
				helper = new MySQLiteHelper(MainView.getContext(), DBNAME,
						false);
				db = helper.getWritableDatabase();
			}
		} else {
			System.out.println("Databse is ready");
		}
		helper.readyDatabase(db);
		cacheLock = new ReentrantLock();

		tripCache = new Cache<String, Trip>(4000);
	}

	private synchronized void populateStopCache() {
		try {
			Cursor c = db.rawQuery("SELECT * FROM stops", new String[] {});
			stopCache = new HashMap<String, Stop>((int) (c.getCount() * 1.5));
			c.moveToFirst();
			while (!c.isAfterLast()) {
				Stop s = new Stop(c.getString(0), c.getString(1), c.getInt(2),
						c.getInt(3));
				stopCache.put(s.getId(), s);
				c.moveToNext();
			}
			c.close();

		} catch (Exception e) {
			System.out.println("Error in populateStopCache()");
			e.printStackTrace();
		}
	}

	/**
	 * Returns the location of all grids in which a given route is listed for
	 * the route search.
	 * 
	 * @param routeId
	 *            Route to look up.
	 * @return The rounded locations of each grid square.
	 */
	public List<Point> routeSearchGrids(String routeId) {
		Cursor c = db.rawQuery(
				"SELECT lat, lon FROM route_search WHERE route_id = '"
						+ routeId + "';", new String[] {});
		List<Point> points = new ArrayList<Point>(c.getCount());
		c.moveToFirst();
		while (!c.isAfterLast()) {
			points.add(new Point(c.getInt(1), c.getInt(0)));
			c.moveToNext();
		}
		return points;
	}

	/**
	 * Returns a list of routes which are near a given location.
	 * 
	 * @param lat
	 *            Location in decimal degrees.
	 * @param lon
	 *            Location in decimal degrees.
	 * @return List of nearby routes.
	 */
	public List<Route> routeSearch(int lat, int lon) {
		Cursor c = db.rawQuery("SELECT routes.* "
				+ "FROM routes, route_search WHERE " + "route_search.lat = "
				+ lat + " AND route_search.lon = " + lon
				+ " AND routes._id = route_search.route_id;", new String[] {});
		c.moveToFirst();
		List<Route> routeList = new ArrayList<Route>(c.getCount());
		while (!c.isAfterLast()) {
			Route r = new Route(c.getString(0), c.getString(1), c.getString(2),
					c.getString(3), c.getInt(4));
			routeList.add(r);
			c.moveToNext();
		}

		return routeList;
	}

	/**
	 * Generates the part of the WHERE clause dealing with dates.
	 * "... AND start_date <= currentDate AND end_date >= currentDate AND day = 1"
	 * 
	 * @return part of the where clause for finding trips
	 */
	private String dateSQL() {
		Calendar cal = GregorianCalendar.getInstance();
		String currentDate = String.format("%04d%02d%02d",
				cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1,
				cal.get(Calendar.DAY_OF_MONTH));

		String startDate = "AND start_date <= " + currentDate;
		String endDate = " AND end_date >= " + currentDate + " ";

		switch (cal.get(Calendar.DAY_OF_WEEK)) {
		case Calendar.MONDAY:
			return " AND mon = 1 " + startDate + endDate;
		case Calendar.TUESDAY:
			return " AND tue = 1 " + startDate + endDate;
		case Calendar.WEDNESDAY:
			return " AND wed = 1 " + startDate + endDate;
		case Calendar.THURSDAY:
			return " AND thu = 1 " + startDate + endDate;
		case Calendar.FRIDAY:
			return " AND fri = 1 " + startDate + endDate;
		case Calendar.SATURDAY:
			return " AND sat = 1 " + startDate + endDate;
		case Calendar.SUNDAY:
			return " AND sun = 1 " + startDate + endDate;
		default:
			return "";
		}
	}

	/**
	 * Returns all stops in the system
	 * 
	 * @return A list of all stops
	 */
	public synchronized List<Stop> getAllStops() {
		if (stopCache == null) {
			populateStopCache();
		}
		return new ArrayList<Stop>(stopCache.values());
	}

	/**
	 * Returns a single stop matching the given Id, null if none found.
	 * 
	 * @param id
	 *            The ID to look up.
	 * @return The stop with the ID provided, null if nothing found.
	 */
	public synchronized Stop getStopById(String id) {
		if (stopCache == null) {
			populateStopCache();
		}
		return stopCache.get(id);
	}

	/**
	 * Returns all routes in the database.
	 * 
	 * @return List of routes in the database
	 */
	public List<Route> getAllRoutes() {
		List<Route> routes = null;
		try {
			Cursor c = db.rawQuery("SELECT * FROM routes", new String[] {});
			routes = new ArrayList<Route>(c.getCount());
			c.moveToFirst();
			while (!c.isAfterLast()) {
				Route r = new Route(c.getString(0), c.getString(1),
						c.getString(2), c.getString(3), c.getInt(4));
				routes.add(r);
				c.moveToNext();
			}
			c.close();

		} catch (Exception e) {
			System.out.println("Error in getAllRoutes()");
			e.printStackTrace();
			return null;
		}
		return routes;
	}

	/**
	 * Returns a single route matching the given Id, null if none found.
	 * 
	 * @param id
	 *            The ID to look up.
	 * @return The route with the ID provided, null if nothing found.
	 */
	public Route getRouteById(String id) {
		id = Utilities.escapeChar(id);
		Route r = null;
		try {
			Cursor c = db.rawQuery("SELECT * FROM routes WHERE _id = '" + id
					+ "'", new String[] {});
			if (c.getCount() > 0) {
				// System.out.println("Found result: " + c.getCount());
				c.moveToNext();
				r = new Route(c.getString(0), c.getString(1), c.getString(2),
						c.getString(3), c.getInt(4));
			}
			c.close();

		} catch (Exception e) {
			System.out.println("Error in getRouteById()");
			e.printStackTrace();
			return null;
		}
		return r;
	}

	/**
	 * Returns all trips in the database.
	 * 
	 * @return List of trips in the database
	 */
	public List<Trip> getAllTrips() {
		List<Trip> trips = null;
		try {
			Cursor c = db.rawQuery("SELECT * FROM trips", new String[] {});

			trips = new ArrayList<Trip>(c.getCount());
			c.moveToFirst();
			while (!c.isAfterLast()) {
				Trip t = loadTrip(c.getString(0));
				if (t == null) {
					t = new Trip(c.getString(0), c.getString(1),
							c.getString(2), c.getString(3), c.getString(4),
							c.getInt(5), c.getInt(6));
					cacheTrip(t);
				}
				// check if this trip is valid today.
				// if (checkDate(db, t))
				trips.add(t);
				c.moveToNext();
			}
			c.close();

		} catch (Exception e) {
			System.out.println("Error in getAllTrips()");
			e.printStackTrace();
			return null;
		}
		return trips;
	}

	/**
	 * Returns a single trip matching the given Id, null if none found.
	 * 
	 * @param id
	 *            The ID to look up.
	 * @return The trip with the ID provided, null if nothing found.
	 */
	public Trip getTripById(String id) {
		id = Utilities.escapeChar(id);
		Trip t = loadTrip(id);
		if (t == null) {
			try {
				Cursor c = db.rawQuery("SELECT * FROM trips WHERE _id = '" + id
						+ "'", new String[] {});
				if (c.getCount() > 0) {
					// System.out.println("Found result: " + c.getCount());
					c.moveToNext();
					t = new Trip(c.getString(0), c.getString(1),
							c.getString(2), c.getString(3), c.getString(4),
							c.getInt(5), c.getInt(6));
					cacheTrip(t);
				}
				c.close();

			} catch (Exception e) {
				System.out.println("Error in getRouteById()");
				e.printStackTrace();
				return null;
			}
		}
		return t;
	}

	/**
	 * Returns a list of trips which are part of the given route, empty list if
	 * none found. Trips are ordered by start time.
	 * 
	 * @param id
	 *            The route to match.
	 * @return The trips with the route ID provided, empty list if nothing
	 *         found.
	 */
	public List<Trip> getTripsByRoute(String routeId) {
		routeId = Utilities.escapeChar(routeId);
		List<Trip> trips = null;
		try {
			// Cursor c = db
			// .rawQuery(
			// "SELECT trips._id, route_id, service_id, block_id, "
			// + "trip_headsign, start_time FROM trips, calendar WHERE "
			// + "calendar._id = trips.service_id AND "
			// + "route_id = '" + routeId + "' "
			// + dateSQL() + " ORDER BY start_time",
			// new String[] {});

			Cursor c = db.rawQuery("SELECT * FROM trips, calendar WHERE "
					+ "calendar._id = trips.service_id AND " + "route_id = '"
					+ routeId + "' " + dateSQL() + " ORDER BY start_time",
					new String[] {});

			trips = new ArrayList<Trip>(c.getCount());

			c.moveToFirst();
			while (!c.isAfterLast()) {
				Trip t = loadTrip(c.getString(0));
				if (t == null) {
					t = new Trip(c.getString(0), c.getString(1),
							c.getString(2), c.getString(3), c.getString(4),
							c.getInt(5), c.getInt(6));
					cacheTrip(t);
				}

				// if (checkDate(db, t))
				trips.add(t);
				c.moveToNext();
			}
			c.close();

		} catch (Exception e) {
			System.out.println("Error in getTripsByRoute()");
			e.printStackTrace();
			return null;
		}
		return trips;
	}

	/**
	 * Returns a list of trips which are starting in the given interval, empty
	 * list if none found. Trips are ordered by start time.
	 * 
	 * @param id
	 *            The time interval to search in.
	 * @return The trips with the route ID provided, empty list if nothing
	 *         found.
	 */
	public List<Trip> getTripsByStartTime(long intervalStart, long intervalEnd) {
		List<Trip> trips = null;
		try {

			Cursor c = db.rawQuery("SELECT * FROM trips, calendar WHERE "
					+ "calendar._id = trips.service_id AND " + "start_time >= "
					+ intervalStart + " AND start_time <= " + intervalEnd
					+ dateSQL() + " ORDER BY start_time", new String[] {});

			trips = new ArrayList<Trip>(c.getCount());

			c.moveToFirst();
			while (!c.isAfterLast()) {
				Trip t = loadTrip(c.getString(0));
				if (t == null) {
					t = new Trip(c.getString(0), c.getString(1),
							c.getString(2), c.getString(3), c.getString(4),
							c.getInt(5), c.getInt(6));
					cacheTrip(t);
				}

				// if (checkDate(db, t))
				trips.add(t);
				c.moveToNext();
			}
			c.close();

		} catch (Exception e) {
			System.out.println("Error in getTripsByRoute()");
			e.printStackTrace();
			return null;
		}
		return trips;
	}

	/**
	 * Returns a list of trips which are running in the given interval, empty
	 * list if none found. Trips are ordered by start time.
	 * 
	 * @param id
	 *            The time interval to search in.
	 * @return The trips with the route ID provided, empty list if nothing
	 *         found.
	 */
	public List<Trip> getTripsByTime(long intervalStart, long intervalEnd) {
		List<Trip> trips = null;
		try {

			Cursor c = db.rawQuery("SELECT * FROM trips, calendar WHERE "
					+ "calendar._id = trips.service_id AND ((start_time <= "
					+ intervalStart + " AND end_time >= " + intervalStart
					+ ") OR (start_time <= " + intervalEnd
					+ " AND end_time >= " + intervalEnd
					+ " ) OR ( start_time >= " + intervalStart
					+ " AND end_time <= " + intervalEnd + ")) " + dateSQL()
					+ " ORDER BY start_time", new String[] {});

			trips = new ArrayList<Trip>(c.getCount());

			c.moveToFirst();
			while (!c.isAfterLast()) {
				Trip t = loadTrip(c.getString(0));
				if (t == null) {
					t = new Trip(c.getString(0), c.getString(1),
							c.getString(2), c.getString(3), c.getString(4),
							c.getInt(5), c.getInt(6));
					cacheTrip(t);
				}
				trips.add(t);
				c.moveToNext();
			}
			c.close();

		} catch (Exception e) {
			System.out.println("Error in getTripsByRoute()");
			e.printStackTrace();
			return null;
		}
		return trips;
	}

	/**
	 * Returns a list of trips which are part of the given block (each trip
	 * follows one after the other), empty list if none found. Trips are ordered
	 * by start time.
	 * 
	 * @param id
	 *            The block to match.
	 * @return The trips with the block ID provided, empty list if nothing
	 *         found.
	 */
	public List<Trip> getTripsByBlock(String blockId) {
		blockId = Utilities.escapeChar(blockId);
		List<Trip> trips = null;
		try {
			Cursor c = db.rawQuery("SELECT * FROM trips WHERE block_id = '"
					+ blockId + "' ORDER BY start_time", new String[] {});
			trips = new ArrayList<Trip>(c.getCount());
			c.moveToFirst();
			while (!c.isAfterLast()) {
				Trip t = loadTrip(c.getString(0));
				if (t == null) {
					t = new Trip(c.getString(0), c.getString(1),
							c.getString(2), c.getString(3), c.getString(4),
							c.getInt(5), c.getInt(6));
					cacheTrip(t);
				}
				trips.add(t);
				c.moveToNext();
			}
			c.close();

		} catch (Exception e) {
			System.out.println("Error in getTripsByBlock()");
			e.printStackTrace();
			return null;
		}
		return trips;
	}

	/**
	 * Returns a list of timed stops which are part of the given trip, empty
	 * list if none found. Stops are ordered by sequence.
	 * 
	 * @param id
	 *            The route to match.
	 * @return The timed stops with the trip ID provided, empty list if nothing
	 *         found.
	 */
	public List<TimedStop> getTimedStopsByTrip(String tripId) {
		tripId = Utilities.escapeChar(tripId);
		List<TimedStop> stops = null;
		try {
			Cursor c = db
					.rawQuery(
							"SELECT * FROM stop_times WHERE trip_id = ? ORDER BY stop_sequence",
							new String[] { String.valueOf(tripId) });
			stops = new ArrayList<TimedStop>(c.getCount());
			c.moveToFirst();
			while (!c.isAfterLast()) {
				TimedStop s = new TimedStop(c.getInt(0), c.getString(1),
						c.getInt(2), c.getInt(3), c.getString(4), c.getInt(5));
				stops.add(s);
				c.moveToNext();
			}
			c.close();

		} catch (Exception e) {
			System.out.println("Error in getTimedStopsByTrip()");
			e.printStackTrace();
			return null;
		}
		return stops;
	}

	/**
	 * Provides a list of all stops in an area. Will swap values if min/max are
	 * reversed. This is not supported by the stop cache and requires database
	 * seaches.
	 * 
	 * @param minX
	 *            Minimum longitude to include.
	 * @param maxX
	 *            Maximum longitude to include.
	 * @param minY
	 *            Minimum latitude to include.
	 * @param maxY
	 *            Maximum latitude to include.
	 * @return A list of stops, empty list if none found.
	 */
	public List<Stop> getStopsByArea(int minX, int maxX, int minY, int maxY) {
		// System.out.println("Get by area: " + minX + ", " + maxX + ", " + minY
		// + ", " + maxY);
		int temp;
		if (minX > maxX) {
			temp = minX;
			minX = maxX;
			maxX = temp;
		}
		if (minY > maxY) {
			temp = minY;
			minY = maxY;
			maxY = temp;
		}

		List<Stop> stops = null;
		try {
			Cursor c = db.rawQuery("SELECT * FROM stops WHERE lon BETWEEN "
					+ minX + " AND " + maxX + " AND lat BETWEEN " + minY
					+ " AND " + maxY, new String[] {});
			stops = new ArrayList<Stop>(c.getCount());
			c.moveToFirst();
			while (!c.isAfterLast()) {
				Stop s = new Stop(c.getString(0), c.getString(1), c.getInt(2),
						c.getInt(3));
				stops.add(s);
				c.moveToNext();
			}
			c.close();

		} catch (Exception e) {
			System.out.println("Error in getStopsByArea()");
			e.printStackTrace();
			return null;
		}
		return stops;
	}

	/**
	 * Returns a list of all non-timing waypoints for a trip and sequence
	 * number. Empty list if none found. Sorted by sequence/subsequence number.
	 * 
	 * @param tripId
	 *            The trip to look up
	 * @param sequenceNumber
	 *            The sequence number inside the trip to look up
	 * @return A list of waypoints for that sequence/trip combo, empty list if
	 *         nothing found.
	 */
	public List<WayPoint> getWayPointsByTrip(String tripId, int sequenceNumber) {
		tripId = Utilities.escapeChar(tripId);
		List<WayPoint> wayPoints = null;
		try {
			Cursor c = db
					.rawQuery(
							"SELECT * FROM waypoint_data WHERE trip_id = ? AND sequence_number = ? ORDER BY sequence_number, subsequence_number",
							new String[] { String.valueOf(tripId),
									String.valueOf(sequenceNumber) });
			wayPoints = new ArrayList<WayPoint>(c.getCount());
			c.moveToFirst();
			while (!c.isAfterLast()) {
				WayPoint wp = new WayPoint(c.getInt(0), c.getString(1),
						c.getInt(2), c.getInt(3), c.getInt(4), c.getInt(5), -1,
						-1);
				wayPoints.add(wp);
				c.moveToNext();
			}
			c.close();

		} catch (Exception e) {
			System.out.println("Error in getTimedStopsByTrip()");
			e.printStackTrace();
			return null;
		}
		return wayPoints;
	}

	/**
	 * Adds or updates a waypoint for a trip.
	 * 
	 * @param wp
	 *            Waypoint to add.
	 */
	public void saveWayPoint(WayPoint wp) {
		try {
			db.execSQL("INSERT OR REPLACE INTO waypoint_data (trip_id, sequence_number, "
					+ "subsequence_number, lat, lon VALUES ('"
					+ wp.getTripId()
					+ "','"
					+ wp.getSequence()
					+ ","
					+ wp.getSubSequence()
					+ "," + wp.getLat() + "," + wp.getLon() + ");");
		} catch (Exception e) {
			System.out.println("Error in addWayPoint()");
			e.printStackTrace();
		}
	}

	// public void saveWayPoint(WayPoint wp) {
	// if (wp.getId() == -1) {
	// addWayPoint(wp);
	// } else {
	// try {
	// ContentValues args = new ContentValues();
	// args.put("value", String.valueOf(wp.getValue()));
	// args.put("update_date", String.valueOf(wp.getDayUpdated()));
	// db.update("waypoint_data", args, "_id = ?",
	// new String[] { String.valueOf(wp.getId()) });
	// } catch (Exception e) {
	// System.out.println("Error in addWayPoint()");
	// e.printStackTrace();
	// }
	// }
	// }

	/**
	 * Returns the travel time for a grid point from the database for a trip.
	 * 
	 * @param id
	 *            The ID of the trip
	 * @param lat
	 *            The location in rounded decimal degrees
	 * @param lon
	 *            The location in rounded decimal degrees
	 * @return The travel time for the grid square.
	 */
	public int getTravelTimeForLocation(String id, int lat, int lon) {
		id = Utilities.escapeChar(id);
		int result = -1;
		try {
			Cursor c = db.rawQuery("SELECT travel_time FROM trip_data WHERE "
					+ "trip_id = ? AND lat = ? AND lon = ?", new String[] { id,
					String.valueOf(lat), String.valueOf(lon) });

			if (c.getCount() > 0) {
				c.moveToFirst();
				result = c.getInt(0);
			}
			c.close();

		} catch (Exception e) {
			System.out.println("Error in getTripData()");
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Returns a map of all travel times for grid points along a trip.
	 * 
	 * @param tripId
	 *            The Id of the trip
	 * @return A hashMap of all travel times. Key is Point<lat,lon> which are
	 *         rounded decimal degrees.
	 */
	public HashMap<Point, Integer> getTravelTimeForTrip(String tripId) {
		tripId = Utilities.escapeChar(tripId);
		HashMap<Point, Integer> times = null;
		try {
			Cursor c = db
					.rawQuery(
							"SELECT lat, lon, travel_time FROM trip_data WHERE trip_id = ?",
							new String[] { String.valueOf(tripId) });
			times = new HashMap<Point, Integer>(c.getCount());
			c.moveToFirst();
			while (!c.isAfterLast()) {
				Point p = new Point(c.getInt(0), c.getInt(1));
				times.put(p, c.getInt(0));
				c.moveToNext();
			}
			c.close();

		} catch (Exception e) {
			System.out.println("Error in getTimedStopsByTrip()");
			e.printStackTrace();
			return null;
		}
		return times;
	}

	private void cacheTrip(Trip t) {
		if (enableCache) {
			cacheLock.lock();
			tripCache.put(t.getId(), t);
			cacheLock.unlock();
		}
	}

	private Trip loadTrip(String tripId) {
		Trip t = null;
		if (enableCache) {
			cacheLock.lock();
			t = tripCache.get(tripId);
			cacheLock.unlock();
		}
		return t;
	}

	public void enableCache() {
		enableCache = true;
	}

	public void disableCache() {
		enableCache = false;
		tripCache.clear();
	}

	public String getConfig(String configId) {
		Cursor c = db.rawQuery("SELECT * FROM config WHERE _id = '" + configId
				+ "';", new String[] {});
		c.moveToFirst();
		if (c.getCount() > 0) {
			return c.getString(1);
		} else
			return "";
	}

	public void setConfig(String configId, String configVal) {
		db.execSQL("INSERT OR REPLACE INTO config (_id, val) VALUES ('"
				+ configId + "','" + configVal + "');");
	}
}
