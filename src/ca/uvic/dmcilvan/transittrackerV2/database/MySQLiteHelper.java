package ca.uvic.dmcilvan.transittrackerV2.database;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.content.ContentValues;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.media.MediaScannerConnection;
import android.os.Environment;
import ca.uvic.dmcilvan.transittrackerV2.MainView;
import ca.uvic.dmcilvan.transittrackerV2.entity.ActiveTrip;
import ca.uvic.dmcilvan.transittrackerV2.entity.PathSegment;
import ca.uvic.dmcilvan.transittrackerV2.entity.TimedStop;
import ca.uvic.dmcilvan.transittrackerV2.entity.Trip;
import ca.uvic.dmcilvan.transittrackerV2.utilities.Utilities;

/**
 * @author Daniel
 * 
 */
public class MySQLiteHelper extends SQLiteOpenHelper {

	private boolean writeToDownloads = false;

	private String dbName;
	private static final int DBVERSION = 1;
	private String curStatus = "Waiting";
	// MainView context;

	private int databaseSize = 1;
	private int linesRead = 0;
	private int tablesEntered = 0;
	private final int TOTALSTEPS = 7;
	private double averageLat = 0D;
	private double averageLon = 0D;
	private double lonScale = 1D;

	int minX, maxX, minY, maxY;

	private HashSet<String> routeSet;
	private HashSet<String> tripSet;
	private HashSet<String> stopSet;

	SQLiteDatabase db;

	public MySQLiteHelper(MainView context, String dbName, boolean writeToFile) {
		super(context, dbName, null, DBVERSION);
		this.dbName = dbName;
		updateStatus("Created Helper");
		this.writeToDownloads = writeToFile;
	}

	public String getCurrentActivity() {
		return curStatus;
	}

	@Override
	public void onOpen(SQLiteDatabase db) {
		this.db = db;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		this.db = db;

		updateStatus("Starting DB Table Creation");

		// Foreign keys create too large a slow down, needs to be handled
		// by program.
		// db.execSQL("PRAGMA foreign_keys=ON;");

		db.execSQL("CREATE TABLE config(_id TEXT PRIMARY KEY, val TEXT);");
		db.execSQL("INSERT INTO config (_id,val) VALUES ('init','" + 0 + "');");

		System.out.println("Creating table 'stops'");
		db.execSQL("CREATE TABLE stops(_id TEXT PRIMARY KEY, stop_name TEXT, lat INTEGER, lon INTEGER);");

		System.out.println("Creating table 'routes'");
		db.execSQL("CREATE TABLE routes(_id TEXT PRIMARY KEY, route_short_name TEXT,"
				+ " route_long_name TEXT, route_desc TEXT, max_travel_time INT);");

		System.out.println("Creating table 'trips'");
		// Foreign key constraints handled by temporary table when reading from
		// text files.
		db.execSQL("CREATE TABLE trips(_id TEXT PRIMARY KEY UNIQUE,route_id TEXT NOT NULL, "
				+ "service_id TEXT NOT NULL, block_id TEXT, trip_headsign TEXT, "
				+ "start_time INT NOT NULL, end_time INT NOT NULL);");

		// Foreign key constraints handled by temporary table when reading from
		// text files.
		System.out.println("Creating table 'stop_times'");
		db.execSQL("CREATE TABLE stop_times(_id INTEGER PRIMARY KEY AUTOINCREMENT, trip_id TEXT NOT NULL, "
				+ "arrival_time INTEGER NOT NULL, departure_time INTEGER NOT NULL, stop_id TEXT NOT NULL, "
				+ "stop_sequence INTEGER NOT NULL);");

		System.out.println("Creating table 'time_blocks'");
		db.execSQL("CREATE TABLE time_blocks (_id INTEGER PRIMARY KEY, start_time TEXT NOT NULL, "
				+ " end_time TEXT NOT NULL);");

		System.out.println("Creating table 'trip_data'");
		db.execSQL("CREATE TABLE trip_data (_id INTEGER PRIMARY KEY AUTOINCREMENT, trip_id TEXT NOT NULL, "
				+ "time_block INTEGER NOT NULL, lat INTEGER NOT NULL, lon INTEGER NOT NULL, "
				+ "travel_time INTEGER NOT NULL, FOREIGN KEY(trip_id) REFERENCES trips(_id), "
				+ "FOREIGN KEY(time_block) REFERENCES time_blocks(_id));");

		System.out.println("Creating table 'waypoint_data'");
		db.execSQL("CREATE TABLE waypoint_data (_id INTEGER PRIMARY KEY AUTOINCREMENT, trip_id TEXT NOT NULL, "
				+ "sequence_number INTEGER, subsequence_number INTEGER, lat INTEGER, lon INTEGER, "
				+ " FOREIGN KEY(trip_id) REFERENCES trips(_id));");

		System.out.println("Creating table 'calendar'");
		db.execSQL("CREATE TABLE calendar (_id TEXT PRIMARY KEY, mon INT NOT NULL, tue INT NOT NULL, wed INT NOT NULL, "
				+ "thu INT NOT NULL, fri INT NOT NULL, sat INT NOT NULL, sun INT NOT NULL, start_date INT NOT NULL, "
				+ "end_date INT NOT NULL);");

		System.out.println("Creating table 'route_search'");
		db.execSQL("CREATE TABLE route_search (_id INT PRIMARY KEY, lat INT NOT NULL, lon INT NOT NULL, "
				+ "route_id TEXT NOT NULL, FOREIGN KEY (route_id) REFERENCES routes(_id), "
				+ "UNIQUE(lat, lon, route_id) ON CONFLICT IGNORE);");

		// Create Indexes
		db.execSQL("CREATE INDEX stops_idx ON stops (_id);");
		db.execSQL("CREATE INDEX routes_idx ON routes (_id);");
		db.execSQL("CREATE INDEX trip_data_idx on trip_data (trip_id,time_block,lat,lon);");
		db.execSQL("CREATE INDEX waypoint_data_idx ON waypoint_data (trip_id);");
		db.execSQL("CREATE INDEX stop_times_idx on stop_times (trip_id);");
		db.execSQL("CREATE INDEX route_search_idx ON route_search(lat,lon);");
		db.execSQL("CREATE INDEX trips_idx_block ON trips (block_id);");
		db.execSQL("CREATE INDEX trips_idx_route ON trips (route_id);");
		db.execSQL("CREATE INDEX trips_idx_start_time ON trips (start_time);");
	}

	private String getConfig(String configId) {
		Cursor c = db.rawQuery("SELECT * FROM config WHERE _id = '" + configId
				+ "';", new String[] {});
		c.moveToFirst();
		if (c.getCount() > 0) {
			return c.getString(1);
		} else
			return "";
	}

	private void setConfig(String configId, String configVal) {
		db.execSQL("INSERT OR REPLACE INTO config (_id, val) VALUES ('"
				+ configId + "','" + configVal + "');");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

	private void updateStatus(final String update) {
		MainView.getContext().runOnUiThread(new Runnable() {
			public void run() {
				MainView.getContext().updateAppStatus(update);
			}
		});
	}

	/**
	 * Readies a database for use, and optionally writes a copy to external
	 * storage/Downloads.
	 * 
	 * @param db
	 */
	public void readyDatabase(SQLiteDatabase db) {
		if (this.db == null)
			this.db = db;
		int i = Integer.parseInt(getConfig("init"));
		System.out.println("init is " + i);

		if (i < TOTALSTEPS) {
			findDataInfo();
		}
		tablesEntered++;
		try {

			if (i == 0) {
				populateStops();
				setConfig("init", String.valueOf(++i));
			}
			tablesEntered++;
			if (i == 1) {
				populateRoutes();
				setConfig("init", String.valueOf(++i));
			}
			tablesEntered++;
			if (i == 2) {
				populateCalendar();
				setConfig("init", String.valueOf(++i));
			}
			tablesEntered++;
			if (i == 3) {
				populateTrips();
				setConfig("init", String.valueOf(++i));
			}
			tablesEntered++;
			if (i == 4) {
				populateStopTimes();
				setConfig("init", String.valueOf(++i));
			}
			tablesEntered++;
			if (i == 5) {
				updateTrips();
				setConfig("init", String.valueOf(++i));
			}
			tablesEntered++;
			if (i == 6) {
				generateRouteSearch();
				setConfig("init", String.valueOf(++i));
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		tablesEntered++;

		// Write the database to the publicly accessible area of memory.
		if (writeToDownloads) {
			writeToPublicFolder();
		}
	}

	private void writeToPublicFolder() {
		// Optimize the space the database uses first.
		MainView.getContext().updateAppStatus(
				"Optimizing database before writing to public folder");
		// db.execSQL("VACUUM;");

		File path = Environment
				.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
		File file = new File(path, dbName);
		if (file.exists())
			file.delete();
		if (!path.canWrite())
			System.out.println("Can't write!");
		try {
			path.mkdirs();
			if (path.canWrite()) {
				file.createNewFile();
				MainView.getContext().updateAppStatus(
						"Writing database file to "
								+ file.getAbsolutePath().toString());
				InputStream is = new FileInputStream(MainView.getContext()
						.getDatabasePath(dbName));
				OutputStream os = new FileOutputStream(file);
				byte buffer[] = new byte[1024];
				int length;
				while ((length = is.read(buffer)) > 0) {
					os.write(buffer, 0, length);
				}
				is.close();
				os.flush();
				os.close();
				System.out.println("File size: " + file.length());
				// Intent intent = new Intent(
				// Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
				// intent.setData(Uri.fromFile(file));
				// MainView.getContext().sendBroadcast(intent);
				MediaScannerConnection.scanFile(MainView.getContext(),
						new String[] { file.toString() }, null, null);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void findDataInfo() {
		List<String> files = new ArrayList<String>();
		files.add("calendar");
		files.add("calendar_dates");
		files.add("routes");
		files.add("stop_times");
		files.add("stops");
		files.add("trips");
		updateStatus("Examining database");
		int latLocation = -1;
		int lonLocation = -1;
		int numberStops = 0;

		for (String s : files) {

			InputStream is;
			try {
				AssetManager am = MainView.getContext().getAssets();
				is = am.open(s);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			try {
				String line = br.readLine();
				while (line != null) {
					databaseSize++;

					// Each trip needs to be updated again later and also need
					// to do search generation, so count
					// twice.
					if (s.equals("trips")) {
						databaseSize += 2;
					}
					// Need to know the average latitude of stops for the
					// projection to Cartesian system, may as well get it now.
					if (s.equals("stops")) {
						String vals[] = line.split(",");
						// grab the location of the latitude from the first line
						if (latLocation == -1) {
							for (int i = 0; i < vals.length; i++) {
								if (vals[i].compareTo("stop_lat") == 0)
									latLocation = i;
								if (vals[i].compareTo("stop_lon") == 0)
									lonLocation = i;
							}
						} else {
							try {
								averageLat += Double
										.parseDouble(vals[latLocation]);
								averageLon += Double
										.parseDouble(vals[lonLocation]);
								numberStops++;
							} catch (NumberFormatException e) {
								System.out.println("Failed to parse latitude");
							}
						}
					}
					line = br.readLine();
				}
				br.close();
			} catch (Exception e2) {
				throw new RuntimeException(e2);
			}
		}

		averageLat /= numberStops;
		averageLon /= numberStops;
		lonScale = Math.cos(Math.PI * averageLat / 180);
		System.out.println("Found average lat of " + averageLat);
		System.out.println("Found average lon of " + averageLon);
		System.out.println("Found scale factor of " + lonScale);

		setConfig("averageLat", "" + averageLat);
		setConfig("averageLon", "" + averageLon);
		setConfig("longitudeScale", "" + lonScale);
	}

	private void generateRouteSearch() {
		ActiveTrip activeTrip = null;
		HashSet<Tuple> gridSquaresToUpdate = new HashSet<Tuple>(9973);
		Cursor c = db.rawQuery("SELECT * FROM trips", new String[] {});
		c.moveToFirst();
		int i = 0;
		int total = c.getCount();
		while (!c.isAfterLast()) {
			Trip t = new Trip(c.getString(0), c.getString(1), c.getString(2),
					c.getString(3), c.getString(4), c.getInt(5), c.getInt(6));

			// for (Trip t : getAllTrips()) {
			// activeTrip = new ActiveTrip(getRouteById(t.getId()), t,
			// getTimedStopsByTrip(t.getId()));
			activeTrip = new ActiveTrip(null, t, null);

			System.out.println("Route:" + t.getRouteId() + ", trip:"
					+ t.getId() + ", Size:" + gridSquaresToUpdate.size());
			for (PathSegment s : activeTrip.getPath()) {
				Tuple newContainer = new Tuple(s.lat, s.lon, t.getRouteId());
				gridSquaresToUpdate.add(newContainer);
			}

			updateStatus("Creating Route Search Info " + stats() + ": "
					+ t.getRouteId() + "(" + (i++) + "/" + total + ")");

			if (Utilities.getCurrentMem() > Utilities.MAX_MEMORY
					|| gridSquaresToUpdate.size() > 5000) {
				writeSearchInfo(gridSquaresToUpdate);
				gridSquaresToUpdate = new HashSet<Tuple>(9973);
			}
			c.moveToNext();
		}

		writeSearchInfo(gridSquaresToUpdate);

		setConfig("generateRouteSearch", "true");
		c.close();
	}

	private void writeSearchInfo(Set<Tuple> gridSquaresToUpdate) {
		System.out.println("Writing to database!");
		SQLiteStatement insert = db
				.compileStatement("INSERT INTO route_search (lat, lon, route_id) "
						+ "VALUES (?,?,?)");
		db.beginTransaction();
		for (Tuple container : gridSquaresToUpdate) {
			insert.bindLong(1, container.lat);
			insert.bindLong(2, container.lon);
			insert.bindString(3, container.routeId);
			insert.executeInsert();
		}
		db.setTransactionSuccessful();
		db.endTransaction();
		// Cursor c = db.rawQuery("SELECT * FROM route_search", null);
		// c.moveToFirst();
		// while (!c.isAfterLast()) {
		// System.out.println(c.getString(1) + ", " + c.getString(2) + ", "
		// + c.getString(3));
		// c.moveToNext();
		// }
		System.gc();
	}

	private void updateTrips() {

		db.beginTransaction();

		// String sql =
		// "UPDATE trips SET start_time = ?, end_time = ? WHERE _id = ?";
		// SQLiteStatement update = db.compileStatement(sql);

		int i = 0;

		Cursor c = db.rawQuery(
				"SELECT _id, route_id FROM trips ORDER BY route_id",
				new String[] {});
		c.moveToFirst();
		while (!c.isAfterLast()) {

			int startTime, endTime;
			String trip_id = c.getString(0);
			String route_id = c.getString(1);
			List<TimedStop> stops = getTimedStopsByTrip(trip_id);
			if (stops.size() > 0) {
				startTime = stops.get(0).getDepartureTime();
				endTime = stops.get(stops.size() - 1).getArrivalTime();
				System.out.println("Update trip: " + trip_id + ", route_id: "
						+ route_id + " with time:" + startTime);

				ContentValues args = new ContentValues();
				args.put("start_time", String.valueOf(startTime));
				args.put("end_time", String.valueOf(endTime));
				db.update("trips", args, "_id = ?", new String[] { trip_id });

				int totalTravelTime = endTime - startTime;
				args.clear();
				args.put("max_travel_time", totalTravelTime);
				db.update(
						"routes",
						args,
						"_id = ? AND max_travel_time < ?",
						new String[] { route_id,
								String.valueOf(totalTravelTime) });

				// db.execSQL("UPDATE trips SET start_time = ? WHERE _id = ?;",
				// new String[] { String.valueOf(startTime), t.getId() });
				updateStatus("Updating Trips " + stats() + ": " + route_id
						+ "(" + (i++) + ")");
				c.moveToNext();
			}
		}
		db.setTransactionSuccessful();
		db.endTransaction();
	}

	private List<TimedStop> getTimedStopsByTrip(String tripId) {
		tripId = Utilities.escapeChar(tripId);
		List<TimedStop> stops = null;
		try {
			Cursor c = db
					.rawQuery(
							"SELECT * FROM stop_times WHERE trip_id = ? ORDER BY stop_sequence",
							new String[] { String.valueOf(tripId) });
			stops = new ArrayList<TimedStop>(c.getCount());
			c.moveToFirst();
			while (!c.isAfterLast()) {
				TimedStop s = new TimedStop(c.getInt(0), c.getString(1),
						c.getInt(2), c.getInt(3), c.getString(4), c.getInt(5));
				stops.add(s);
				c.moveToNext();
			}
			c.close();

		} catch (Exception e) {
			System.out.println("Error in getTimedStopsByTrip()  SQLHelper");
			e.printStackTrace();
			return null;
		}
		return stops;
	}

	private void populateStops() {
		db.beginTransaction();

		stopSet = new HashSet<String>();

		InputStream is;
		try {
			AssetManager am = MainView.getContext().getAssets();
			is = am.open("stops");
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String line;
		try {
			// figure out where everything is.
			int idLoc = -1, nameLoc = -1, latLoc = -1, lonLoc = -1;
			String vals[];

			line = br.readLine();
			vals = line.split(",");
			for (int i = 0; i < vals.length; i++) {
				if (vals[i].compareTo("stop_id") == 0)
					idLoc = i;
				if (vals[i].compareTo("stop_name") == 0)
					nameLoc = i;
				if (vals[i].compareTo("stop_lat") == 0)
					latLoc = i;
				if (vals[i].compareTo("stop_lon") == 0)
					lonLoc = i;
			}

			if (idLoc == -1 || nameLoc == -1 || latLoc == -1 || lonLoc == -1) {
				// TODO error, invalid db values!
				System.out.println("Invalid database input");
				return;
			}

			line = br.readLine();
			while (line != null) {
				vals = line.split(",");
				updateStatus("Adding Stops " + stats() + ": " + vals[nameLoc]
						+ "(" + vals[idLoc] + ")");

				// Change so there are no negatives
				double rawLat, rawLon;
				rawLat = Double.parseDouble(vals[latLoc]);
				rawLon = Double.parseDouble(vals[lonLoc]);
				double deltaLon = (averageLon - rawLon) * lonScale;

				int lat, lon;
				lat = (int) ((rawLat + 360.0) * Utilities.COORDMULT);
				lon = (int) ((averageLon - deltaLon + 360.0) * Utilities.COORDMULT);

				if (lat < minY) {
					minY = lat;
				}
				if (lat > maxY) {
					maxY = lat;
				}
				if (lon < minX) {
					minX = lon;
				}
				if (lon > maxX) {
					maxX = lon;
				}
				try {
					stopSet.add(Utilities.escapeChar(vals[idLoc]));
					db.execSQL("INSERT INTO stops (_id, stop_name, lat, lon) VALUES ('"
							+ Utilities.escapeChar(vals[idLoc])
							+ "','"
							+ Utilities.escapeChar(vals[nameLoc])
							+ "',"
							+ lat
							+ "," + lon + ");");
					line = br.readLine();
				} catch (SQLException e2) {
				}
			}

			db.execSQL("INSERT INTO config (_id,val) VALUES ('maxX','" + maxX
					+ "');");
			db.execSQL("INSERT INTO config (_id,val) VALUES ('maxY','" + maxY
					+ "');");
			db.execSQL("INSERT INTO config (_id,val) VALUES ('minX','" + minX
					+ "');");
			db.execSQL("INSERT INTO config (_id,val) VALUES ('minY','" + minY
					+ "');");

			br.close();
			db.setTransactionSuccessful();
		} catch (Exception e2) {
			throw new RuntimeException(e2);
		} finally {
			db.endTransaction();
		}
	}

	private void populateRoutes() {
		db.beginTransaction();

		routeSet = new HashSet<String>();

		InputStream is;
		try {
			AssetManager am = MainView.getContext().getAssets();
			is = am.open("routes");
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String line;
		try {
			// figure out where everything is.
			int idLoc = -1, sNameLoc = -1, lNameLoc = -1, descLoc = -1;
			String vals[];

			line = br.readLine();
			vals = line.split(",");
			for (int i = 0; i < vals.length; i++) {
				if (vals[i].compareTo("route_id") == 0)
					idLoc = i;
				if (vals[i].compareTo("route_short_name") == 0)
					sNameLoc = i;
				if (vals[i].compareTo("route_long_name") == 0)
					lNameLoc = i;
				if (vals[i].compareTo("route_desc") == 0)
					descLoc = i;
			}

			if (idLoc == -1 || sNameLoc == -1 || lNameLoc == -1
					|| descLoc == -1) {
				// TODO error, invalid db values!
				System.out.println("Invalid database input");
				return;
			}

			line = br.readLine();
			while (line != null) {
				try {
					vals = line.split(",");
					updateStatus("Adding Routes " + stats() + ": "
							+ vals[lNameLoc] + "(" + vals[idLoc] + ")");
					db.execSQL("INSERT INTO routes VALUES ('"
							+ Utilities.escapeChar(vals[idLoc]) + "','"
							+ Utilities.escapeChar(vals[sNameLoc]) + "','"
							+ Utilities.escapeChar(vals[lNameLoc]) + "','"
							+ Utilities.escapeChar(vals[descLoc]) + "', 0);");
					routeSet.add(Utilities.escapeChar(vals[idLoc]));
					line = br.readLine();
				} catch (SQLException e2) {
					e2.printStackTrace();
				}
			}
			br.close();
			db.setTransactionSuccessful();
		} catch (Exception e2) {
			throw new RuntimeException(e2);
		} finally {
			db.endTransaction();
		}
	}

	private void populateTrips() {
		db.beginTransaction();

		tripSet = new HashSet<String>();
		if (routeSet == null) {
			try {
				Cursor c = db.rawQuery("SELECT * FROM routes", new String[] {});
				routeSet = new HashSet<String>(c.getCount() * 2);
				c.moveToFirst();
				while (!c.isAfterLast()) {
					routeSet.add(c.getString(0));
					c.moveToNext();
				}
				c.close();

			} catch (Exception e) {
				System.out.println("Error in populateTrips()");
				e.printStackTrace();
			}
		}

		InputStream is;
		try {
			AssetManager am = MainView.getContext().getAssets();
			is = am.open("trips");
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String line;
		try {

			db.execSQL("CREATE TABLE TEMP_trips(_id TEXT PRIMARY KEY,route_id TEXT NOT NULL, "
					+ "service_id TEXT, block_id TEXT, trip_headsign TEXT, start_time INT, end_time INT, "
					+ "FOREIGN KEY(route_id) REFERENCES routes(_id), "
					+ "FOREIGN KEY(service_id) REFERENCES calendar(_id));");

			// figure out where everything is.
			int routeIdLoc = -1, serviceIdLoc = -1, blockIdLoc = -1, tripIdLoc = -1, headsignLoc = -1;
			String vals[];

			line = br.readLine();
			vals = line.split(",");
			for (int i = 0; i < vals.length; i++) {
				if (vals[i].compareTo("route_id") == 0)
					routeIdLoc = i;
				if (vals[i].compareTo("service_id") == 0)
					serviceIdLoc = i;
				if (vals[i].compareTo("block_id") == 0)
					blockIdLoc = i;
				if (vals[i].compareTo("trip_id") == 0)
					tripIdLoc = i;
				if (vals[i].compareTo("trip_headsign") == 0)
					headsignLoc = i;
			}

			if (routeIdLoc == -1 || serviceIdLoc == -1 || blockIdLoc == -1
					|| tripIdLoc == -1 || headsignLoc == -1) {
				// TODO error, invalid db values!
				System.out.println("Invalid database input");
				return;
			}

			line = br.readLine();

			int i = 0;
			String lastRouteId = "";
			while (line != null) {
				vals = line.split(",");

				if (!routeSet.contains(Utilities.escapeChar(vals[routeIdLoc]))) {
					line = br.readLine();
					continue;
				}
				updateStatus("Adding Trips " + stats() + ": "
						+ vals[routeIdLoc] + "(" + (i++) + ")");
				lastRouteId = vals[routeIdLoc];

				String sql = "INSERT INTO TEMP_trips (_id,route_id, service_id, block_id, trip_headsign, start_time, end_time) VALUES ('"
						+ Utilities.escapeChar(vals[tripIdLoc])
						+ "','"
						+ Utilities.escapeChar(vals[routeIdLoc])
						+ "','"
						+ Utilities.escapeChar(vals[serviceIdLoc])
						+ "','"
						+ Utilities.escapeChar(vals[blockIdLoc]) + "', ";
				if (vals.length == 5)
					sql += "'" + Utilities.escapeChar(vals[headsignLoc])
							+ "', ";
				else
					sql += "'NO HEADSIGN LISTED',";
				sql += "-1,-1);";
				try {
					db.execSQL(sql);
					tripSet.add(Utilities.escapeChar(vals[tripIdLoc]));
				} catch (SQLException e2) {
					e2.printStackTrace();
				}
				line = br.readLine();
			}
			br.close();

			db.execSQL("INSERT INTO trips SELECT * FROM TEMP_trips ORDER BY route_id");
			// db.execSQL("INSERT INTO trips SELECT * FROM TEMP_trips");
			db.execSQL("DROP TABLE temp_trips");

			db.setTransactionSuccessful();
		} catch (Exception e2) {
			e2.printStackTrace();
			throw new RuntimeException(e2);
		} finally {
			db.endTransaction();
		}

		routeSet = null;

	}

	private void populateStopTimes() {
		db.beginTransaction();

		if (tripSet == null) {
			try {
				Cursor c = db.rawQuery("SELECT * FROM trips", new String[] {});
				tripSet = new HashSet<String>(c.getCount() * 2);
				c.moveToFirst();
				while (!c.isAfterLast()) {
					tripSet.add(c.getString(0));
					c.moveToNext();
				}
				c.close();

			} catch (Exception e) {
				System.out.println("Error in populateTrips()");
				e.printStackTrace();
			}
		}

		if (stopSet == null) {
			try {
				Cursor c = db.rawQuery("SELECT * FROM stops", new String[] {});
				stopSet = new HashSet<String>(c.getCount() * 2);
				c.moveToFirst();
				while (!c.isAfterLast()) {
					stopSet.add(c.getString(0));
					c.moveToNext();
				}
				c.close();

			} catch (Exception e) {
				System.out.println("Error in populateTrips()");
				e.printStackTrace();
			}
		}

		InputStream is;
		try {
			AssetManager am = MainView.getContext().getAssets();
			is = am.open("stop_times");
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String line;
		try {

			db.execSQL("CREATE TABLE TEMP_stop_times(_id INTEGER PRIMARY KEY AUTOINCREMENT, trip_id TEXT NOT NULL, "
					+ "arrival_time INTEGER NOT NULL, departure_time INTEGER NOT NULL, stop_id TEXT NOT NULL, "
					+ "stop_sequence INTEGER NOT NULL, "
					+ "FOREIGN KEY(trip_id) REFERENCES TEMP_trips(_id), "
					+ "FOREIGN KEY(stop_id) REFERENCES stops(_id));");

			// figure out where everything is.
			int tripIdLoc = -1, arrivalLoc = -1, departureLoc = -1, seqLoc = -1, stopIdLoc = -1;
			String vals[];

			line = br.readLine();
			vals = line.split(",");
			for (int i = 0; i < vals.length; i++) {
				if (vals[i].compareTo("trip_id") == 0)
					tripIdLoc = i;
				if (vals[i].compareTo("arrival_time") == 0)
					arrivalLoc = i;
				if (vals[i].compareTo("departure_time") == 0)
					departureLoc = i;
				if (vals[i].compareTo("stop_sequence") == 0)
					seqLoc = i;
				if (vals[i].compareTo("stop_id") == 0)
					stopIdLoc = i;

			}

			if (tripIdLoc == -1 || arrivalLoc == -1 || departureLoc == -1
					|| seqLoc == -1 || stopIdLoc == -1) {
				// TODO error, invalid db values!
				System.out.println("Invalid database input");
				return;
			}

			line = br.readLine();

			String lastTripId = "";
			String sql = "INSERT INTO TEMP_stop_times (trip_id, arrival_time, departure_time, stop_id, stop_sequence)";
			sql += " VALUES ( ?, ?, ?, ?, ?)";
			SQLiteStatement insert = db.compileStatement(sql);
			while (line != null) {
				vals = line.split(",");
				if (!tripSet.contains(Utilities.escapeChar(vals[tripIdLoc]))
						|| !stopSet.contains(Utilities
								.escapeChar(vals[stopIdLoc]))) {
					line = br.readLine();
					continue;
				}
				updateStatus("Adding Timing Info " + stats() + ": "
						+ vals[tripIdLoc]);
				lastTripId = vals[tripIdLoc];

				insert.bindString(1, Utilities.escapeChar(vals[tripIdLoc]));
				insert.bindLong(2,
						Utilities.convertTimeSToMSec(vals[arrivalLoc]));
				insert.bindLong(3,
						Utilities.convertTimeSToMSec(vals[departureLoc]));
				insert.bindString(4, Utilities.escapeChar(vals[stopIdLoc]));
				insert.bindLong(5, Integer.parseInt(vals[seqLoc]));
				try {
					insert.executeInsert();
				} catch (SQLException e2) {
					e2.printStackTrace();
				}

				// db.execSQL("INSERT INTO TEMP_stop_times (trip_id, arrival_time, departure_time, stop_id, stop_sequence) VALUES ('"
				// + Utilities.escapeChar(vals[tripIdLoc])
				// + "',"
				// + Utilities.convertTimeSToUSec(vals[arrivalLoc])
				// + ","
				// + Utilities.convertTimeSToUSec(vals[departureLoc])
				// + ",'" + vals[stopIdLoc] + "'," + vals[seqLoc] + ");");

				// insert.executeInsert();

				line = br.readLine();
			}
			br.close();

			updateStatus("Sorting Timing Info " + stats());

			db.execSQL("INSERT INTO stop_times SELECT * FROM TEMP_stop_times ORDER BY trip_id");
			// db.execSQL("INSERT INTO stop_times SELECT * FROM TEMP_stop_times");
			db.execSQL("DROP TABLE TEMP_stop_times");

			db.setTransactionSuccessful();

		} catch (Exception e2) {
			throw new RuntimeException(e2);
		} finally {
			db.endTransaction();
		}

		tripSet = null;
		stopSet = null;

	}

	private void populateCalendar() {
		db.beginTransaction();
		InputStream is;
		try {
			AssetManager am = MainView.getContext().getAssets();
			is = am.open("calendar");
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String line;
		try {
			// figure out where everything is.
			int idLoc = -1, monLoc = -1, tueLoc = -1, wedLoc = -1, thuLoc = -1;
			int friLoc = -1, satLoc = -1, sunLoc = -1, startDateLoc = -1, endDateLoc = -1;
			String vals[];

			line = br.readLine();
			vals = line.split(",");
			for (int i = 0; i < vals.length; i++) {
				if (vals[i].compareTo("service_id") == 0)
					idLoc = i;
				if (vals[i].compareTo("monday") == 0)
					monLoc = i;
				if (vals[i].compareTo("tuesday") == 0)
					tueLoc = i;
				if (vals[i].compareTo("wednesday") == 0)
					wedLoc = i;
				if (vals[i].compareTo("thursday") == 0)
					thuLoc = i;
				if (vals[i].compareTo("friday") == 0)
					friLoc = i;
				if (vals[i].compareTo("saturday") == 0)
					satLoc = i;
				if (vals[i].compareTo("sunday") == 0)
					sunLoc = i;
				if (vals[i].compareTo("start_date") == 0)
					startDateLoc = i;
				if (vals[i].compareTo("end_date") == 0)
					endDateLoc = i;

			}

			if (idLoc == -1 || monLoc == -1 || tueLoc == -1 || wedLoc == -1
					|| thuLoc == -1 || friLoc == -1 || satLoc == -1
					|| sunLoc == -1 || startDateLoc == -1 || endDateLoc == -1) {
				// TODO error, invalid db values!
				System.out.println("Invalid database input");
				return;
			}

			line = br.readLine();

			while (line != null) {
				vals = line.split(",");
				updateStatus("Adding Calendar Info " + stats() + ": "
						+ vals[idLoc]);
				db.execSQL("INSERT INTO calendar VALUES ('"
						+ Utilities.escapeChar(vals[idLoc]) + "',"
						+ vals[monLoc] + "," + vals[tueLoc] + ","
						+ vals[wedLoc] + "," + vals[thuLoc] + ","
						+ vals[friLoc] + "," + vals[satLoc] + ","
						+ vals[sunLoc] + "," + vals[startDateLoc] + ","
						+ vals[endDateLoc] + ");");
				line = br.readLine();
			}
			br.close();
			db.setTransactionSuccessful();
		} catch (Exception e2) {
			throw new RuntimeException(e2);
		} finally {
			db.endTransaction();
		}
	}

	private String stats() {
		int percentComplete = ((linesRead++) * 100) / databaseSize;
		return "(" + (tablesEntered) + "/" + TOTALSTEPS + ")   ["
				+ percentComplete + "%]";
	}

	/**
	 * 
	 * For use in the HashSet which stores information for
	 * generateRouteSearch().
	 * 
	 * @author Daniel
	 * 
	 */
	private class Tuple {
		int lat;
		int lon;
		String routeId;

		public Tuple(int lat, int lon, String routeId) {
			this.lat = lat;
			this.lon = lon;
			this.routeId = routeId;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + lat;
			result = prime * result + lon;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Tuple other = (Tuple) obj;
			if (lat != other.lat)
				return false;
			if (lon != other.lon)
				return false;
			if (routeId == null) {
				if (other.routeId != null)
					return false;
			} else if (!routeId.equals(other.routeId))
				return false;
			return true;
		}
	}

	public boolean isDatabaseReady() {
		return Integer.parseInt(getConfig("init")) >= TOTALSTEPS;
	}

	public boolean downloadDatabase(String fileURL) {
		if (writeToDownloads) {
			System.out.println("Writing fresh database to file");
			return false;
		}
		URL url;
		try {
			url = new URL(fileURL);
		} catch (MalformedURLException e) {
			System.out.println("Bad URL for db download.");
			return false;
		}
		try {
			File cachedCopy = new File(MainView.getContext().getCacheDir()
					+ "tempDB.db");
			cachedCopy.delete();
			cachedCopy.createNewFile();

			URLConnection connection = url.openConnection();
			connection.connect();
			long fileLength = connection.getContentLength();
			InputStream input = new BufferedInputStream(url.openStream());
			OutputStream output = new FileOutputStream(cachedCopy);

			byte buffer[] = new byte[1024];
			int length;
			long total = 0;
			long lastUpdate = 0;
			while ((length = input.read(buffer)) > 0) {
				output.write(buffer, 0, length);
				total += length;
				lastUpdate += length;
				if (lastUpdate >= 104857 || total == fileLength) {
					lastUpdate = 0;
					updateStatus("Downloading database from WIFI:\n"
							+ (total / 1048576) + "MB/"
							+ (fileLength / 1048576) + "MB ["
							+ (total * 100 / fileLength) + "%]");
				}
			}
			input.close();
			output.flush();
			output.close();

			db.close();
			this.close();

			System.out.println("Copying database from cache");
			File database = MainView.getContext().getDatabasePath(dbName);
			database.delete();
			database.createNewFile();
			input = new FileInputStream(cachedCopy);
			output = new FileOutputStream(database);

			while ((length = input.read(buffer)) > 0) {
				output.write(buffer, 0, length);
			}
			System.out.println("Finished copying");
			input.close();
			output.flush();
			output.close();
			cachedCopy.delete();

			db = SQLiteDatabase.openOrCreateDatabase(
					database.getAbsolutePath(), null);
			return true;

		} catch (IOException e) {
			System.out.println("Failed to download database");
			return false;
		}
	}
}
