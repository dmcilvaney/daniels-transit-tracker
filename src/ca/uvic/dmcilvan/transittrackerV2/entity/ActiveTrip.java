package ca.uvic.dmcilvan.transittrackerV2.entity;

import java.util.ArrayList;
import java.util.List;

import ca.uvic.dmcilvan.transittrackerV2.database.DatabaseManager;
import ca.uvic.dmcilvan.transittrackerV2.utilities.Utilities;


public class ActiveTrip {
	Trip trip;
	Route route;
	List<TimedStop> timedStops;
	// HashMap<String, Stop> stopMap; // Hashmap of all stops currently in this
	// route.
	List<Stop> stopList;
	List<PathSegment> path;

	int theoreticalPoint = 0;

	public ActiveTrip(Trip trip) {
		this(null, trip, null);
	}

	public ActiveTrip(Route route, Trip trip, List<TimedStop> timedStops) {
		if (timedStops == null) {
			timedStops = DatabaseManager.getInstance().getTimedStopsByTrip(
					trip.getId());
		}
		this.stopList = null;
		this.route = route;
		this.trip = trip;
		this.timedStops = timedStops;
		this.path = trip.getPath();
		// this.path = null;
	}

	public Trip getTrip() {
		return trip;
	}

	public Route getRoute() {
		return route;
	}

	public List<Stop> getStops() {
		if (stopList == null) {
			this.stopList = new ArrayList<Stop>(timedStops.size());
			for (TimedStop ts : timedStops) {
				Stop newStop = DatabaseManager.getInstance().getStopById(
						ts.getStopId());
				if (newStop != null)
					stopList.add(newStop);
			}
		}
		return stopList;
	}

	/**
	 * Calculates and returns the theoretics location of a bus on this trip.
	 * 
	 * @return Returns the grid square corresponding to that location
	 */
	public PathSegment getTheoreticalPathSegment() {
		if (getPath().size() > 0)
			return path.get(getTheoreticalIndex());
		else
			return null;
	}

	/**
	 * Calculates and returns the theoretics location of a bus on this trip.
	 * 
	 * @return Returns the grid square corresponding to that location
	 */
	public int getTheoreticalIndex() {
		if (path == null)
			path = trip.getPath();
		// generatePath();
		if (path.size() == 0)
			return 0;
		int currentTime = Utilities.getCurrentTime();
		for (int i = theoreticalPoint; i < path.size(); i++) {
			if (path.get(i).arrivalTime < currentTime) {
				theoreticalPoint = i;
			} else {
				break;
			}
		}
		return theoreticalPoint;
	}

	// /**
	// * Generates a list of path segments corresponding the path a bus follows.
	// * It uses extra waypoints if available from the database.
	// */
	// private void generatePath() {
	// // System.out.println("Generating new path");
	// int sizeGuess = timedStops.size();
	// path = new ArrayList<PathSegment>(sizeGuess);
	// HashMap<String, Stop> stopMap = new HashMap<String, Stop>(sizeGuess * 2);
	// for (Stop s : getStops()) {
	// stopMap.put(s.getId(), s);
	// }
	//
	// List<WayPoint> wayPoints = new ArrayList<WayPoint>(sizeGuess * 2);
	//
	// TimedStop prevTimedStop = null;
	// // for (TimedStop ts :
	// // DatabaseManager.getInstance().getTimedStopsByTrip(
	// // trip.getId())) {
	// for (TimedStop ts : timedStops) {
	// if (prevTimedStop == null) {
	// prevTimedStop = ts;
	// }
	//
	// Stop tempStop = stopMap.get(ts.getStopId());
	//
	// wayPoints.add(new WayPoint(-1, trip.getId(), ts.getSequence(), 0,
	// Utilities.roundPos(tempStop.getLat()), Utilities
	// .roundPos(tempStop.getLon()), -1, -1, ts
	// .getArrivalTime(), ts.getDepartureTime()));
	//
	// int newSubSequenceNumber = 1000;
	// for (WayPoint wp : DatabaseManager.getInstance()
	// .getWayPointsByTrip(trip.getId(), ts.getSequence())) {
	//
	// wp.setSubSequenceNumber(newSubSequenceNumber);
	// wayPoints.add(wp);
	// newSubSequenceNumber += 1000;
	// }
	// }
	//
	// // Now have a list of all waypoints, create a full path. Move from the
	// // previous way point to the next way point in the list.
	// WayPoint lastWaypoint = null;
	// PathSegment lastPathSegment = null;
	// int pathSequenceNumber = 0;
	// int timeToWayPoint = 0; // The time to the next timed way point.
	// int distanceToWayPoint = 1; // Distance to the next timed way point (to
	// // allow for timing to be scaled to various
	// // way point distributions.
	// int currentTimeSpent = trip.getStartTime(); // The time at the current
	// // location in the trip.
	//
	// for (int i = 0; i < wayPoints.size(); i++) {
	// WayPoint wp = wayPoints.get(i);
	// int pathSegmentArrivalTime = trip.startTime;
	//
	// if (lastWaypoint == null) {
	// lastWaypoint = wp;
	// continue;
	// }
	//
	// // If we are working from a new timing point, update the time
	// // increment value.
	// if (lastWaypoint.getArrivalTime() != -1
	// && lastWaypoint.getDepartureTime() != -1) {
	//
	// WayPoint prev = lastWaypoint;
	// distanceToWayPoint = 0;
	// timeToWayPoint = 0;
	//
	// for (int j = i; j < wayPoints.size(); j++) {
	// WayPoint next = wayPoints.get(j);
	// distanceToWayPoint += Math.sqrt(Math.pow(
	// (prev.getLat() - next.getLat()), 2)
	// + Math.pow(prev.getLon() - next.getLon(), 2));
	//
	// timeToWayPoint = next.getArrivalTime() - currentTimeSpent;
	//
	// // Found the next timing point. (Keep going until we have a
	// // non zero time if possible)
	// if (next.getArrivalTime() != -1
	// && next.getDepartureTime() != -1
	// && timeToWayPoint > 0) {
	// break;
	// }
	// prev = next;
	// }
	// }
	//
	// int latDiff = wp.getLat() - lastWaypoint.getLat();
	// int lonDiff = wp.getLon() - lastWaypoint.getLon();
	//
	// // If the waypoint is over the same spot just keep going to the next
	// // one.
	// if (latDiff == 0 && lonDiff == 0) {
	// continue;
	// }
	//
	// int increments;
	// int latIncr;
	// int lonIncr;
	// int timeIncr;
	//
	// if (Math.abs(latDiff) > Math.abs(lonDiff)) {
	// increments = Math.abs(latDiff) / Utilities.ROUNDING;
	// } else {
	// increments = Math.abs(lonDiff) / Utilities.ROUNDING;
	// }
	//
	// latIncr = latDiff / increments;
	// lonIncr = lonDiff / increments;
	//
	// if (timeToWayPoint != 0) {
	// Double dist = Math.sqrt(Math.pow(
	// (lastWaypoint.getLat() - wp.getLat()), 2)
	// + Math.pow(lastWaypoint.getLon() - wp.getLon(), 2));
	// timeIncr = (int) (timeToWayPoint * dist / distanceToWayPoint)
	// / increments;
	// } else {
	// timeIncr = 0;
	// }
	//
	// // Fill in the gaps in the temp path.
	// // HashMap<Point, Integer> timeSpentMap = DatabaseManager
	// // .getInstance().getTripData(trip.getId());
	//
	// for (int j = 0; j < increments; j++) {
	// int newLat = Utilities.roundPos(lastWaypoint.getLat() + j
	// * latIncr);
	// int newLon = Utilities.roundPos(lastWaypoint.getLon() + j
	// * lonIncr);
	//
	// // int timeSpent =
	// // DatabaseManager.getInstance().getTripDataForLocation(
	// // trip.getId(), newLat, newLon);
	// // Integer timeSpent = timeSpentMap.get(new Point(newLat,
	// // newLon));
	// int timeSpent = timeIncr;
	//
	// // if (timeSpent == -1)
	// // timeSpent = timeIncr;
	//
	// // if (timeSpent == null)
	// // timeSpent = timeIncr;
	//
	// PathSegment newPathSegment = new PathSegment(newLat, newLon,
	// pathSequenceNumber, timeSpent, lastWaypoint, wp,
	// currentTimeSpent);
	// currentTimeSpent += timeSpent;
	//
	// if (lastPathSegment == null
	// || lastPathSegment.lat != newPathSegment.lat
	// || lastPathSegment.lon != newPathSegment.lon) {
	//
	// path.add(newPathSegment);
	// pathSequenceNumber++;
	// }
	// }
	// lastWaypoint = wp;
	// }
	//
	// }

	// /**
	// * Deletes a generated path if one exists to free memory.
	// */
	// public void releasePath() {
	// path = null;
	// }

	/**
	 * Returns a list of path segments which correspond to the path a bus
	 * follows along the trip. If the path has not been generated before it will
	 * be generated now.
	 * 
	 * @return
	 */
	public List<PathSegment> getPath() {
		return trip.getPath();
	}

	/**
	 * Given a location, returns the index number of the closest path segment in
	 * the path list.
	 * 
	 * @param currentLat
	 *            Current location in decimal degrees.
	 * @param currentLon
	 *            Current location in decimal degrees.
	 * @return Index in the list returned by getPath() of the closest path
	 *         segment to the given location.
	 */
	public int getClosestIndex(int currentLat, int currentLon) {
		int closestIndex = 0;
		double bestDist = Double.MAX_VALUE;

		// activteTripLock.lock();
		List<PathSegment> path = getPath();
		for (int i = 0; i < path.size(); i++) {
			PathSegment p = path.get(i);

			double dist = Math
					.sqrt(Math.pow((p.lat + Utilities.ROUNDING / 2)
							- currentLat, 2)
							+ Math.pow((p.lon + Utilities.ROUNDING / 2)
									- currentLon, 2));
			if (dist < bestDist) {
				bestDist = dist;
				closestIndex = i;
			}
		}
		// activteTripLock.unlock();
		return closestIndex;
	}

	/**
	 * Given a location, returns the closest path segment in the path list.
	 * 
	 * @param currentLat
	 *            Current location in decimal degrees.
	 * @param currentLon
	 *            Current location in decimal degrees.
	 * @return The closest path segment to the given location.
	 */
	public PathSegment getClostestPathSegment(int currentLat, int currentLon) {
		if (getPath().size() > 0)
			return getPath().get(getClosestIndex(currentLat, currentLon));
		else
			return null;
	}
}
