package ca.uvic.dmcilvan.transittrackerV2.entity;

public class PathSegment {
    public int lat;
    public int lon;
    public int seq;
    public int timeSpent;
    public WayPoint nextWayPoint;
    public WayPoint prevWayPoint;
    public int arrivalTime;

    public PathSegment(int lat, int lon, int seq, int timeSpent,
	    WayPoint prevWayPoint, WayPoint nextWayPoint, int arrivalTime) {
	this.lat = lat;
	this.lon = lon;
	this.seq = seq;
	this.timeSpent = timeSpent;
	this.nextWayPoint = nextWayPoint;
	this.prevWayPoint = prevWayPoint;
	this.arrivalTime = arrivalTime;
    }
}
