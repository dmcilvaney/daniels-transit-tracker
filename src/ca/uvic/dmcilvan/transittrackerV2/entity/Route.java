package ca.uvic.dmcilvan.transittrackerV2.entity;

public class Route implements Comparable<Route> {
    String id;
    String shortName;
    String longName;
    String description;
    private int maxTravelTime;

    public Route(String id, String shortName, String longName,
	    String description, int maxTravelTime) {
	this.id = id;
	this.shortName = shortName;
	this.longName = longName;
	this.description = description;
	this.maxTravelTime = maxTravelTime;
    }

    public String toString() {
	String s = "";
	s += "id: " + id;
	s += ", shortName: " + shortName;
	s += ", longName: " + longName;
	s += ", description: " + description;
	return s;
    }

    public String getName() {
	return longName + " (" + shortName + ")";
    }

    public int getMaxTravelTime() {
	return maxTravelTime;
    }

    public String getShortName() {
	return shortName;

    }

    /*
     * (non-Javadoc) Attempts to compare based on route number, goes to string
     * comparison if that fails.
     * 
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Route another) {
	try {
	    return Integer.valueOf(getShortName()).compareTo(
		    Integer.valueOf(another.getShortName()));
	} catch (NumberFormatException e) {
	    return getShortName().compareTo(another.getShortName());
	}
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Route other = (Route) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

    public String getId() {
	return id;
    }
}
