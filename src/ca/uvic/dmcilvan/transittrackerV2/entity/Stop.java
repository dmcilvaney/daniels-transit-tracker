package ca.uvic.dmcilvan.transittrackerV2.entity;

public class Stop {
    String id;
    String name;
    int lat, lon;

    public Stop(String id, String name, int lat, int lon) {
	this.id = id;
	this.name = name;
	this.lat = lat;
	this.lon = lon;
    }

    public String toString() {
	String s = "";
	s += "id: " + id;
	s += ", name: " + name;
	s += ", lat: " + lat;
	s += ", lon: " + lon;
	return s;
    }

    public int getX() {
	return lon;
    }

    public int getY() {
	return lat;
    }

    public int getLat() {
	return getY();
    }

    public int getLon() {
	return getX();
    }

    public String getName() {
	return name;
    }

    public String getId() {
	return id;
    }
}
