package ca.uvic.dmcilvan.transittrackerV2.entity;


public class TimedStop {
    int id;
    String tripId;
    int arrivalTime;
    int departureTime;
    String stopId;
    int sequence;

    public TimedStop(int id, String tripId, int arrivalTime, int departureTime,
	    String stopId, int sequence) {
	this.id = id;
	this.tripId = tripId;
	this.arrivalTime = arrivalTime;
	this.departureTime = departureTime;
	this.stopId = stopId;
	this.sequence = sequence;
    }

    public String toString() {
	return "id: " + id + ", tId:" + tripId + ", arr: " + arrivalTime
		+ ", dep: " + departureTime + ", sId: " + stopId + ", seq: "
		+ sequence;
    }

    public int getDepartureTime() {
	return departureTime;
    }

    public String getStopId() {
	return stopId;
    }

    public int getSequence() {
	return sequence;
    }

    public int getArrivalTime() {
	return arrivalTime;
    }
}
