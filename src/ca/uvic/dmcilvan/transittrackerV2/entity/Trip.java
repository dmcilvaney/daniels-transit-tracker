package ca.uvic.dmcilvan.transittrackerV2.entity;

import java.util.ArrayList;
import java.util.List;

import ca.uvic.dmcilvan.transittrackerV2.database.DatabaseManager;
import ca.uvic.dmcilvan.transittrackerV2.utilities.Utilities;

public class Trip implements Comparable<Trip> {
	String id;
	String routeId;
	String serviceId;
	String blockId;
	String headsign;
	int startTime;
	int endTime;
	public int routeSearchScore;
	List<PathSegment> path;

	public Trip(String id, String routeId, String serviceId, String blockId,
			String headsign, int startTime, int endTime) {
		this.id = id;
		this.routeId = routeId;
		this.serviceId = serviceId;
		this.blockId = blockId;
		this.headsign = headsign;
		this.startTime = startTime;
		this.endTime = endTime;
		this.routeSearchScore = startTime;
	}

	public String toString() {
		return "id: " + id + " route_id: " + routeId + " serviceId: "
				+ serviceId + " block_id: " + blockId;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		if (startTime != -1) {
			return headsign + "(" + Utilities.convertTimeMSecToS(startTime)
					+ " -> " + Utilities.convertTimeMSecToS(endTime) + ")";
		} else {
			return headsign + "(NO START TIME AVAILABLE)";
		}
	}

	public String getRouteId() {
		return routeId;
	}

	public int getStartTime() {
		return startTime;
	}

	public int compareTo(Trip another) {
		if (routeSearchScore == another.routeSearchScore) {
			return startTime - another.startTime;
		} else {
			return routeSearchScore - another.routeSearchScore;
		}
	}

	public String getServiceId() {
		return serviceId;
	}

	public int getEndTime() {
		return endTime;
	}

	public synchronized List<PathSegment> getPath() {
		if (path == null)
			generatePath();
		return path;
	}

	/**
	 * Generates a list of path segments corresponding the path a bus follows.
	 * It uses extra waypoints if available from the database.
	 */
	private void generatePath() {
		// try {
		// System.out.println("*************************Generate sleep");
		// // Thread.sleep(1000);
		// } catch (InterruptedException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// System.out.println("Generating new path");
		DatabaseManager dbm = DatabaseManager.getInstance();
		List<TimedStop> timedStops = dbm.getTimedStopsByTrip(id);
		Trip trip = this;
		int sizeGuess = timedStops.size();
		path = new ArrayList<PathSegment>(sizeGuess);
		List<WayPoint> wayPoints = new ArrayList<WayPoint>(sizeGuess * 2);

		TimedStop prevTimedStop = null;
		for (TimedStop ts : timedStops) {
			if (prevTimedStop == null) {
				prevTimedStop = ts;
			}

			Stop tempStop = dbm.getStopById(ts.getStopId());

			wayPoints.add(new WayPoint(-1, trip.getId(), ts.getSequence(), 0,
					Utilities.roundPos(tempStop.getLat()), Utilities
							.roundPos(tempStop.getLon()), ts.getArrivalTime(),
					ts.getDepartureTime()));

			// load any extra waypoints from the database. (Currently not used)
			// int newSubSequenceNumber = 1000;
			// for (WayPoint wp : dbm.getWayPointsByTrip(trip.getId(),
			// ts.getSequence())) {
			// wp.setSubSequenceNumber(newSubSequenceNumber);
			// wayPoints.add(wp);
			// newSubSequenceNumber += 1000;
			// }
		}

		// Now that we have a list of all waypoints, we can create a full path
		// between them. Move from the previous way point to the next way point
		// in the list filling in segments along the way.
		WayPoint lastWaypoint = null;
		PathSegment lastPathSegment = null;
		int pathSequenceNumber = 0;
		int timeToWayPoint = 0; // The time to the next timed way point.
		int distanceToTimedWayPoint = 1; // Distance to the next timed way point
											// (to
		// allow for timing to be scaled to various
		// waypoint distributions.
		// The time at the current location in the trip.
		int currentTimeSpent = trip.getStartTime();

		for (int mainListIndex = 0; mainListIndex < wayPoints.size(); mainListIndex++) {
			WayPoint wp = wayPoints.get(mainListIndex);

			if (lastWaypoint == null) {
				lastWaypoint = wp;
				continue;
			}

			// If we are working from a new timing point, update the time
			// increment value.
			if (lastWaypoint.getArrivalTime() != -1
					&& lastWaypoint.getDepartureTime() != -1) {
				distanceToTimedWayPoint = 0;
				timeToWayPoint = 0;

				for (int travelTimeIndex = mainListIndex; travelTimeIndex < wayPoints
						.size(); travelTimeIndex++) {
					WayPoint nextTimeWp = wayPoints.get(travelTimeIndex);

					// if the next point doesn't have timing info check the next
					// one. If we run out of points time is still set to zero.
					if (nextTimeWp.getArrivalTime() == -1
							|| nextTimeWp.getDepartureTime() == -1) {
						continue;
					}

					distanceToTimedWayPoint += Math
							.sqrt(Math.pow((lastWaypoint.getLat() - nextTimeWp
									.getLat()), 2)
									+ Math.pow(lastWaypoint.getLon()
											- nextTimeWp.getLon(), 2));

					timeToWayPoint = nextTimeWp.getArrivalTime()
							- currentTimeSpent;

					// Found the next timing point. (Keep going until we have a
					// non zero time if possible)
					if (timeToWayPoint > 0) {
						break;
					}
				}
			}

			int latDiff = wp.getLat() - lastWaypoint.getLat();
			int lonDiff = wp.getLon() - lastWaypoint.getLon();

			// If the waypoint is over the same spot just keep going to the next
			// one.
			if (latDiff == 0 && lonDiff == 0) {
				continue;
			}

			int increments;
			int latIncr;
			int lonIncr;
			int timeIncr;

			if (Math.abs(latDiff) > Math.abs(lonDiff)) {
				increments = Math.abs(latDiff) / Utilities.ROUNDING;
			} else {
				increments = Math.abs(lonDiff) / Utilities.ROUNDING;
			}

			latIncr = latDiff / increments;
			lonIncr = lonDiff / increments;

			if (timeToWayPoint != 0) {
				Double distToWaypoint = Math.sqrt(Math.pow(
						(lastWaypoint.getLat() - wp.getLat()), 2)
						+ Math.pow(lastWaypoint.getLon() - wp.getLon(), 2));
				timeIncr = (int) (timeToWayPoint * distToWaypoint / distanceToTimedWayPoint)
						/ increments;
			} else {
				timeIncr = 0;
			}

			// Fill in the gaps with path segments.
			// Currently not loading modified timing information from the
			// databse.

			// HashMap<Point, Integer> timeSpentMap = DatabaseManager
			// .getInstance().getTripData(trip.getId());

			for (int i = 0; i < increments; i++) {
				int newLat = Utilities.roundPos(lastWaypoint.getLat() + i
						* latIncr);
				int newLon = Utilities.roundPos(lastWaypoint.getLon() + i
						* lonIncr);

				int timeSpent = timeIncr;

				PathSegment newPathSegment = new PathSegment(newLat, newLon,
						pathSequenceNumber, timeSpent, lastWaypoint, wp,
						currentTimeSpent);
				currentTimeSpent += timeSpent;

				if (lastPathSegment == null
						|| lastPathSegment.lat != newPathSegment.lat
						|| lastPathSegment.lon != newPathSegment.lon) {

					path.add(newPathSegment);
					pathSequenceNumber++;
				}
			}
			lastWaypoint = wp;
		}

	}

	/**
	 * Clear the path data structure.
	 */
	public void freePath() {
		path = null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + endTime;
		result = prime * result + startTime;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Trip other = (Trip) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
