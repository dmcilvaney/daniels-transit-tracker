package ca.uvic.dmcilvan.transittrackerV2.entity;

public class TripContainer {
    public Trip trip;
    public float score = 0;
    private String tripId;

    public TripContainer(Trip trip, float score) {
	this.trip = trip;
	if (trip != null)
	    this.tripId = trip.id;
	this.score = score;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((tripId == null) ? 0 : tripId.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	TripContainer other = (TripContainer) obj;
	if (tripId == null) {
	    if (other.tripId != null)
		return false;
	} else if (!tripId.equals(other.tripId))
	    return false;
	return true;
    }
}
