package ca.uvic.dmcilvan.transittrackerV2.entity;

/**
 * 
 * @author Daniel
 * 
 */
public class WayPoint {
	private int id;
	private String tripId;
	private int lat;
	private int lon;
	private int sequence;
	private int subSequence;
	private int arrivalTime;
	private int departureTime;

	public WayPoint(int id, String tripId, int sequence, int subSequence,
			int lat, int lon, int arrivalTime, int departureTime) {
		this.id = id;
		this.tripId = tripId;
		this.lat = lat;
		this.lon = lon;
		this.sequence = sequence;
		this.subSequence = subSequence;
		this.arrivalTime = arrivalTime;
		this.departureTime = departureTime;
	}

	public int getId() {
		return id;
	}

	public String getTripId() {
		return tripId;
	}

	public int getLat() {
		return lat;
	}

	public int getLon() {
		return lon;
	}

	public int getSequence() {
		return sequence;
	}

	public int getSubSequence() {
		return subSequence;
	}

	public int getDepartureTime() {
		return departureTime;
	}

	public int getArrivalTime() {
		return arrivalTime;
	}

	public void setSubSequenceNumber(int newSubSequenceNumber) {
		this.subSequence = newSubSequenceNumber;
	}
}
