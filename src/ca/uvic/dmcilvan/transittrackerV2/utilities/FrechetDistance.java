package ca.uvic.dmcilvan.transittrackerV2.utilities;

import java.util.ArrayList;
import java.util.List;

import ca.uvic.dmcilvan.transittrackerV2.entity.PathSegment;
import ca.uvic.dmcilvan.transittrackerV2.entity.Trip;

public class FrechetDistance {
	public final static int GRIDSIZE = (int) (Utilities.ROUNDING * 1.5);
	public final static int MAXPOINTS = 150;

	List<PathSegment> c1;
	List<PathSegment> c2;

	boolean path[][];
	int checkedAtSize[][];
	boolean usedPath[][];

	int uSize;
	int vSize;
	int curDist;
	int lowDist;
	int highDist;
	private int calls = 0;
	private int maxCalls = 0;
	private boolean failed = false;

	public FrechetDistance(Trip t1, Trip t2) {
		this(t1.getPath(), t2.getPath(), true, true);
		// System.out.println("Done Building");
	}

	public FrechetDistance(List<PathSegment> l1, List<PathSegment> l2) {
		this(l1, l2, true, true);
	}

	public FrechetDistance(List<PathSegment> l1, List<PathSegment> l2,
			boolean rebuild1, boolean rebuild2) {
		// System.out.println("Begining frechet distance calc");
		if (rebuild1)
			l1 = generateFrechetCurve(l1);
		if (rebuild2)
			l2 = generateFrechetCurve(l2);

		int size = l1.size() + l2.size();
		System.out.println("Original size is " + size);
		if (size > MAXPOINTS) {
			float ratio = size / MAXPOINTS;
			l1 = generateFrechetCurve(l1, ratio);
			l2 = generateFrechetCurve(l2, ratio);
		}
		System.out.println("New size is " + (l1.size() + l2.size()));

		if (l1 instanceof ArrayList<?>) {
			c1 = l1;
		} else {
			System.out.println("Frechet needs to build new arrayList");
			c1 = new ArrayList<PathSegment>(l1);
		}
		if (l2 instanceof ArrayList<?>) {
			c2 = l2;
		} else {
			System.out.println("Frechet needs to build new arrayList");
			c2 = new ArrayList<PathSegment>(l2);
		}
		uSize = c1.size();
		vSize = c2.size();
		path = new boolean[uSize][vSize];
		checkedAtSize = new int[uSize][vSize];
		usedPath = new boolean[uSize][vSize];
		// System.out.println("Done Building");
	}

	public int getDist() {
		for (int u = 0; u < uSize; u++) {
			for (int v = 0; v < vSize; v++) {
				path[u][v] = false;
				checkedAtSize[u][v] = -1;
			}
		}
		// System.out.println("Done preparing grid");

		curDist = 1;
		do {
			if (failed) {
				System.out.println("************ FRECHET FAILED");
				return Integer.MAX_VALUE;
			}
			lowDist = curDist;
			curDist *= 2;
			highDist = curDist;
			// System.out.println("Distance is now " + curDist);
			// printC();
			resetPath();
		} while (!buildPath(0, 0, true));
		// printC();
		// System.out
		// .println("####################################################");

		boolean increasingDist = false;
		while (highDist >= lowDist) {
			curDist = (highDist + lowDist) / 2;
			// System.out.println("_________________________________");
			// System.out.println("L:" + lowDist + " C:" + curDist + " H:"
			// + highDist);
			resetPath();
			boolean isPath = buildPath(0, 0, increasingDist);
			if (failed) {
				System.out.println("************ FRECHET FAILED");
				return Integer.MAX_VALUE;
			}
			if (isPath) {
				// System.out.println("Too high");
				highDist = curDist;
				increasingDist = false;
			} else {
				// System.out.println("Too low");
				lowDist = curDist + 1;
				increasingDist = true;
			}
			// printC();
		}
		return curDist;
	}

	private void resetPath() {
		calls = 0;
		for (int i = 0; i < uSize; i++) {
			for (int j = 0; j < vSize; j++) {
				usedPath[i][j] = false;
			}
		}
	}

	private boolean buildPath(int u, int v, boolean increasingDist) {
		calls++;
		if (calls > maxCalls)
			maxCalls = calls;

		if (calls > MAXPOINTS) {
			failed = true;
			return false;
		}

		if (u >= uSize || v >= vSize || checkedAtSize[u][v] == curDist) {
			calls--;
			return false;
		}

		int lastCheck = checkedAtSize[u][v];
		checkedAtSize[u][v] = curDist;
		if (lastCheck >= curDist && !path[u][v]) {
			// Decreasing size and we couldn't access it before so we can't now.
			calls--;
			return false;
		} else if ((lastCheck <= curDist && path[u][v])
				|| dist(u, v) <= curDist) {
			// Increasing size and we could access it before so we can now.
			path[u][v] = true;

			if (u == uSize - 1 && v == vSize - 1) {
				return true;
			} else {
				boolean d1 = false, d2 = false, d3 = false;
				if (buildPath(u + 1, v + 1, increasingDist)) {
					d1 = true;
					usedPath[u + 1][v + 1] = true;
				} else if (buildPath(u + 1, v, increasingDist)) {
					d2 = true;
					usedPath[u + 1][v] = true;
				} else if (buildPath(u, v + 1, increasingDist)) {
					usedPath[u][v + 1] = true;
					d3 = true;
				}
				calls--;
				return d1 || d2 || d3;
			}
		} else {
			// //System.out.println("\tToo far, can't reach here.");
			path[u][v] = false;
			calls--;
			return false;
		}
	}

	/**
	 * Calculates the distance between two points.
	 * 
	 * @param u
	 *            Index of the first point
	 * @param v
	 *            Index of the second point
	 * @return Distance in decimal degrees
	 */
	private int dist(int u, int v) {
		PathSegment p1 = c1.get(u);
		PathSegment p2 = c2.get(v);
		int dLat = p1.lat - p2.lat;
		int dLon = p1.lon - p2.lon;
		long dt = p1.arrivalTime - p2.arrivalTime;
		// Need to convert from milliseconds to a comparable distance, so
		// assuming buses travel at an average speed it is possible to calculate
		// a distance traveled.
		// Convert to decimal degrees traveled.
		dt = Utilities.timeToDist(dt);
		return (int) Math.round(Math.sqrt(Math.pow(dLat, 2) + Math.pow(dLon, 2)
				+ Math.pow(dt, 2)));
	}

	/**
	 * Generates a sparse, discretized curve for use in Frechet distance
	 * calculations using the whole list of vertices provided.
	 * 
	 * @param verts
	 *            Vertices which create a curve in 3d space.
	 * @param l1Ratio
	 * @return A new curve interpolated from the vertices, with larger spacing
	 *         than the normal grid size.
	 */
	public static List<PathSegment> generateFrechetCurve(List<PathSegment> verts) {
		return generateFrechetCurve(verts, 1F);
	}

	/**
	 * Generates a sparse, discretized curve for use in Frechet distance
	 * calculations using only part of the list of vertices provided.
	 * 
	 * @param verts
	 *            Vertices which create a curve in 3d space.
	 * @param firstIndex
	 *            the first vertex to use, indexed from zero.
	 * @param lastIndex
	 *            the last vertex to use, indexed from zero.
	 * @return A new curve interpolated from the vertices, with larger spacing
	 *         than the normal grid size.
	 */
	public static List<PathSegment> generateFrechetCurve(
			List<PathSegment> verts, float ratio) {
		// System.out.println("Generating a new frechet curve from " +
		// firstIndex
		// + " to " + lastIndex + " of size " + verts.size());
		int adjustedGridSize = (int) Math.ceil(GRIDSIZE * ratio);
		PathSegment lP = null;
		List<PathSegment> curve = new ArrayList<PathSegment>();
		for (PathSegment p : verts) {
			if (lP == null) {
				lP = p;
			}

			int dx = p.lat - lP.lat;
			int dy = p.lon - lP.lon;
			int dt = (int) Utilities.timeToDist(p.arrivalTime - lP.arrivalTime);

			if (Math.abs(dx) <= adjustedGridSize
					&& Math.abs(dy) <= adjustedGridSize
					&& Utilities.timeToDist(Math.abs(dt)) <= adjustedGridSize) {
				continue;
			}
			int steps = 0;
			if (Math.abs(dx) >= Math.abs(dy) && Math.abs(dx) >= Math.abs(dt)) {
				steps = dx / adjustedGridSize;
			} else if (Math.abs(dy) >= Math.abs(dx)
					&& Math.abs(dy) >= Math.abs(dt)) {
				steps = dy / adjustedGridSize;
			} else {
				steps = dt / adjustedGridSize;
			}
			steps = Math.abs(steps);
			for (int i = 0; i < steps; i++) {
				curve.add(new PathSegment(lP.lat + dx * i / steps, lP.lon + dy
						* i / steps, -1, -1, null, null, lP.arrivalTime + dt
						* i / steps));
			}

			lP = p;
		}
		PathSegment endPoint = verts.get(verts.size() - 1);
		curve.add(new PathSegment(endPoint.lat, endPoint.lon, -1, -1, null,
				null, endPoint.arrivalTime));

		System.out.println("Coverted " + verts.size() + " points to "
				+ curve.size());
		return curve;
	}
}
