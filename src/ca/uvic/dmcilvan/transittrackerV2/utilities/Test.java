package ca.uvic.dmcilvan.transittrackerV2.utilities;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

import ca.uvic.dmcilvan.transittrackerV2.control.TripSelector;
import ca.uvic.dmcilvan.transittrackerV2.control.TripSelector.TripSelectorCaller;
import ca.uvic.dmcilvan.transittrackerV2.database.DatabaseManager;
import ca.uvic.dmcilvan.transittrackerV2.entity.Trip;


public class Test implements TripSelectorCaller {

	DatabaseManager dbm;
	private LinkedHashMap<String, Double> results;
	private boolean doneWaiting = false;
	private double averageLon;
	private double longitudeScale;
	Thread thisThread;
	private int numResults;

	public Test() {
		dbm = DatabaseManager.getInstance();
		results = new LinkedHashMap<String, Double>();
		averageLon = Double.parseDouble(DatabaseManager.getInstance()
				.getConfig("averageLon"));
		longitudeScale = Double.parseDouble(DatabaseManager.getInstance()
				.getConfig("longitudeScale"));
		thisThread = Thread.currentThread();
	}

	public void run(int shortIterations, int mediumInterations,
			int longIterations) {
		long t1;
		// Test with cache disabled
		dbm.disableCache();

		System.out.println("Starting loadTrip test");
		// load trips. getStartTime() used as lightweight loading.
		t1 = System.currentTimeMillis();
		numResults = dbm.getAllTrips().size();
		for (int i = 0; i < longIterations; i++) {
			for (Trip t : dbm.getAllTrips()) {
				t.getStartTime();
			}
		}
		if (longIterations != 0)
			tag("loadTrip",
					((System.currentTimeMillis() - t1) / (double) longIterations)
							/ numResults);

		System.out.println("Starting createCache test");
		// Test to see how long it takes to add to cache.
		dbm.enableCache();
		t1 = System.currentTimeMillis();
		for (int i = 0; i < longIterations; i++) {
			long t2 = System.currentTimeMillis();
			dbm.disableCache();
			dbm.enableCache();
			long t3 = System.currentTimeMillis();
			t1 += (t3 - t2);
			for (Trip t : dbm.getAllTrips()) {
				t.getStartTime();
			}
			// remove the time taken to clear the cache.
		}
		if (longIterations != 0)
			tag("loadCache",
					((System.currentTimeMillis() - t1) / (double) longIterations)
							/ numResults);

		System.out.println("Starting buildTrip test");
		// Time to build trip structures. Caching disabled so the known time
		// to
		// load trips without caching can be subtracted. Not all trips can be
		// cached at once to results would vary.
		dbm.disableCache();
		t1 = System.currentTimeMillis();
		for (int i = 0; i < longIterations; i++) {
			for (Trip t : dbm.getAllTrips()) {
				t.getPath();
				t.freePath();
			}
		}
		if (longIterations != 0)
			tag("buildTrip",
					((System.currentTimeMillis() - t1) / (double) longIterations)
							/ numResults);

		System.out.println("Starting writeCache test");
		dbm.enableCache();
		int count = 0;
		for (Trip t : dbm.getAllTrips()) {
			t.getPath();
			if (count++ > 10000)
				break;
		}

		t1 = System.currentTimeMillis();
		for (int i = 0; i < longIterations; i++) {
			count = 0;
			for (Trip t : dbm.getAllTrips()) {
				t.getPath();
				if (count++ > 10000)
					break;
			}
		}
		if (longIterations != 0)
			tag("buildTripCached",
					((System.currentTimeMillis() - t1) / (double) longIterations) / 10000);

		System.out.println("Starting tripSelector1 test");
		// Test the trip selector. Run once without timing to make results
		// consistent. Reference trip (#14) is used as the historic path for
		// frechet
		// distances.
		// Uvic is used as base point for lat/lon.
		dbm.disableCache();
		dbm.enableCache();
		double lat = 48.463361D;
		double lon = -123.311813D;
		lon = averageLon - (averageLon - lon) * longitudeScale;
		int convertedLat = (int) (Utilities.COORDMULT * (lat + 360));
		int convertedLon = (int) (Utilities.COORDMULT * (lon + 360));

		TripSelector selector = new TripSelector(convertedLat, convertedLon,
				this);
		selector.findTrip(dbm.getAllRoutes(), 0);
		// Run experiment
		t1 = System.currentTimeMillis();
		for (int i = 0; i < mediumInterations; i++) {
			doneWaiting = false;
			selector.findTrip(dbm.getAllRoutes(), 0);
			while (!doneWaiting) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
				}
			}
		}
		if (mediumInterations != 0)
			tag("tripSearchEuclidean",
					((System.currentTimeMillis() - t1) / (double) mediumInterations));

		System.out.println("Starting tripSelector2 test");
		// Prep Frechet test
		dbm.disableCache();
		dbm.enableCache();
		Trip ref = dbm.getTripById("2474257");
		selector = new TripSelector(ref.getPath(), 0, 0, this);
		selector.findTrip(dbm.getAllRoutes(), 0);
		// Run experiment
		t1 = System.currentTimeMillis();
		for (int i = 0; i < mediumInterations; i++) {
			doneWaiting = false;
			selector.findTrip(dbm.getAllRoutes(), 0);
			while (!doneWaiting) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
				}
			}
		}
		if (mediumInterations != 0)
			tag("tripSearchFrechet",
					((System.currentTimeMillis() - t1) / (double) mediumInterations));

		System.out.println("Starting tripSelector2 test");
		// Prep Frechet test
		dbm.disableCache();
		ref = dbm.getTripById("2474257");
		selector = new TripSelector(ref.getPath(), 0, 0, this);
		selector.findTrip(dbm.getAllRoutes(), 0);
		// Run experiment
		t1 = System.currentTimeMillis();
		for (int i = 0; i < mediumInterations; i++) {
			doneWaiting = false;
			selector.findTrip(dbm.getAllRoutes(), 0);
			while (!doneWaiting) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
				}
			}
		}
		if (mediumInterations != 0)
			tag("tripSearchFrechetNoCache",
					((System.currentTimeMillis() - t1) / (double) mediumInterations));

		dbm.disableCache();
		selector = new TripSelector(convertedLat, convertedLon, this);
		selector.findTrip(dbm.getAllRoutes(), 0);
		// Run experiment
		t1 = System.currentTimeMillis();
		for (int i = 0; i < mediumInterations; i++) {
			doneWaiting = false;
			selector.findTrip(dbm.getAllRoutes(), 0);
			while (!doneWaiting) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
				}
			}
		}
		if (mediumInterations != 0)
			tag("tripSearchEuclideanNoCache",
					((System.currentTimeMillis() - t1) / (double) mediumInterations));

		// Test the speed of get current time.
		t1 = System.currentTimeMillis();
		for (int i = 0; i < shortIterations; i++) {
			Utilities.getCurrentTime();
		}
		if (shortIterations != 0)
			tag("getTime", (System.currentTimeMillis() - t1)
					/ (double) shortIterations);

		// Test the speed of get memory.
		t1 = System.currentTimeMillis();
		for (int i = 0; i < shortIterations; i++) {
			Utilities.getCurrentMem();
		}
		if (shortIterations != 0)
			tag("getMem", (System.currentTimeMillis() - t1)
					/ (double) shortIterations);

		// Test the speed of get memory.
		t1 = System.currentTimeMillis();
		for (int i = 0; i < shortIterations; i++) {
			Utilities.getMaxMem();
		}
		if (shortIterations != 0)
			tag("getMaxMem", (System.currentTimeMillis() - t1)
					/ (double) shortIterations);

		System.out.println("Done testing");
		Entry<String, Long> lastE = null;
		for (Entry<String, Double> e : results.entrySet()) {
			System.out.println(e.getKey() + ": " + e.getValue()
					+ " MS per opperation");
		}
	}

	private void tag(String tag, double time) {
		results.put(tag, time);
	}

	public void handleSortedTrips(List<Trip> sortedTrips) {
		doneWaiting = true;
		numResults = sortedTrips.size();
		thisThread.interrupt();
	}
}
