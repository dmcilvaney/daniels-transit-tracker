package ca.uvic.dmcilvan.transittrackerV2.utilities;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import android.app.ActivityManager;
import android.content.Context;

import ca.uvic.dmcilvan.transittrackerV2.MainView;

public class Utilities {

	// How many metres to round coordinates to.
	private static final int ROUNDING_METERS = 200;
	// Make coordinate integers as large as possible.
	// public static final int COORDMULT = Integer.MAX_VALUE / 100000;
	public static final int COORDMULT = 111000;
	public static final int MAX_MEMORY = 14 * 1048576;
	// Conversion from metres to decimal degrees.
	public static final int ROUNDING = (int) (ROUNDING_METERS * (COORDMULT / 111000D));
	// Average speed a bus travels at in km/h.
	public static final int AVERAGE_SPEED = 10;
	// Multiply times in milliseconds by this to get distances.
	// Calculated by tms*h/ms*km/h*deg/km*AVGSPEED*COORDMULT;
	private static final long TIME_CONVERSION_FACTOR = 399600000;

	/**
	 * Converts a time in milliseconds to a distance traveled in that time at
	 * AVERAGE_SPEED km/h.
	 * 
	 * @param time
	 *            The travel time in milliseconds
	 * @return The distance traveled in decimal degrees.
	 */
	public static long timeToDist(long time) {
		return (time * AVERAGE_SPEED * COORDMULT) / TIME_CONVERSION_FACTOR;
	}

	/**
	 * For use in SQL, inserts escape characters into strings.
	 * 
	 * @param input
	 * @return
	 */
	public static String escapeChar(String input) {
		return input.replace("'", "''");
	}

	/**
	 * Takes a time in string form ('HH:mm:ss') and converts it to one in
	 * milliseconds.
	 * 
	 * @param timeString
	 *            String ('HH:mm:ss') to convert.
	 * @return Time in milliseconds from midnight.
	 */
	public static int convertTimeSToMSec(String timeString) {
		int hours = Integer.parseInt(timeString.split(":")[0]);
		int minutes = Integer.parseInt(timeString.split(":")[1]);
		int seconds = Integer.parseInt(timeString.split(":")[2]);
		return seconds * 1000 + minutes * 60 * 1000 + hours * 60 * 60 * 1000;
	}

	/**
	 * Takes a time in milliseconds from midnight and converts it to a string
	 * ('HH:mm:ss').
	 * 
	 * @param timeInUSec
	 *            time since midnight to convert.
	 * @return Time in string form ('HH:mm:ss')
	 */
	public static String convertTimeMSecToS(int timeInUSec) {
		try {
			String sign = "";
			if (timeInUSec < 0) {
				sign += "-";
				timeInUSec *= -1;
			}
			Calendar c = GregorianCalendar.getInstance();
			c.clear();
			c.set(Calendar.MILLISECOND, timeInUSec);
			SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
			return sign + df.format(c.getTime());
		} catch (Exception e) {
			return "Unable to format date!";
		}
	}

	/**
	 * Returns the number of days since epoch.
	 * 
	 * @return Days since epoch.
	 */
	public static int getCurrentDay() {
		Calendar c = GregorianCalendar.getInstance();
		long millis = c.getTimeInMillis();
		return (int) (millis / (1000 * 60 * 60 * 24));
	}

	/**
	 * Returns the current time in milliseconds from midnight.
	 * 
	 * @return The number of milliseconds from midnight.
	 */
	public static int getCurrentTime() {
		Calendar c = GregorianCalendar.getInstance();
		int time = c.get(Calendar.HOUR_OF_DAY) * 60 * 60 * 1000;
		time += c.get(Calendar.MINUTE) * 60 * 1000;
		time += c.get(Calendar.SECOND) * 1000;
		time += c.get(Calendar.MILLISECOND);

		// Continue past midnight. Assume all busses are done running by 3:00
		// AM.
		if (time < 10800000) {
			time += 86400000;
		}

		return time;

		// return convertTimeSToUSec("12:00:00");
	}

	/**
	 * Rounds a decimal degree coordinate to the grid size.
	 * 
	 * @param coord
	 *            Coordinate to round
	 * @return Rounded coordinate
	 */
	public static int roundPos(int coord) {
		coord /= ROUNDING;
		coord *= ROUNDING;
		return coord;
	}

	/**
	 * Returns the current memory usage in bytes
	 * 
	 * @return Heap size of the current app in bytes
	 */
	public static long getCurrentMem() {
		long usedSize = 0L;
		long totalSize = 0L;
		long freeSize = 0L;

		try {
			Runtime info = Runtime.getRuntime();
			freeSize = info.freeMemory();
			totalSize = info.totalMemory();
			usedSize = totalSize - freeSize;
			// System.out.println("MEMORY: total: " + (totalSize/ 1048576L) +
			// " used: "
			// + (usedSize/ 1048576L) + " free:" + (freeSize/ 1048576L));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return usedSize;
	}

	/**
	 * Maximum size the app should grow to. This is 75% of the hard limit as
	 * given by Runtime.maxMemory() or ActivityManager.getMemoryClass(), which
	 * ever is smaller.
	 * 
	 * @return Bytes the app can use in memory before it should limit its
	 *         growth.
	 */
	public static long getMaxMem() {
		Context context = MainView.getContext();

		if (context != null) {
			long maxMem = Runtime.getRuntime().maxMemory();
			long memClass = ((ActivityManager) context
					.getSystemService(Context.ACTIVITY_SERVICE))
					.getMemoryClass();
			maxMem = (long) (Math.min(maxMem, memClass * 1048576) * 0.75F);
			return maxMem;
			// return (long) (30 * 1048576);
		} else {
			return (long) (16 * 0.8F * 1048576);
		}

	}
}
