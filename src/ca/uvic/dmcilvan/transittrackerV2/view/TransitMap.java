package ca.uvic.dmcilvan.transittrackerV2.view;

import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

import ca.uvic.dmcilvan.transittrackerV2.MainView;
import ca.uvic.dmcilvan.transittrackerV2.control.ActiveTripController;
import ca.uvic.dmcilvan.transittrackerV2.control.ActiveTripControllerObserver;
import ca.uvic.dmcilvan.transittrackerV2.control.Bus;
import ca.uvic.dmcilvan.transittrackerV2.database.DatabaseManager;
import ca.uvic.dmcilvan.transittrackerV2.entity.PathSegment;
import ca.uvic.dmcilvan.transittrackerV2.entity.Route;
import ca.uvic.dmcilvan.transittrackerV2.entity.Stop;
import ca.uvic.dmcilvan.transittrackerV2.entity.TimedStop;
import ca.uvic.dmcilvan.transittrackerV2.entity.Trip;
import ca.uvic.dmcilvan.transittrackerV2.utilities.Utilities;


public class TransitMap extends View implements ActiveTripControllerObserver {

	private static float MIN_ZOOM = 100f;
	private static float MAX_ZOOM = 100000f;
	private float scaleFactor = 100f;
	private ScaleGestureDetector detector;

	private boolean active = false;

	private int viewArea = 100; // size of area to draw in decimal degrees *
	private int drawMult = 1;// coordMult.
	private int width = 1;
	private int height = 1;
	private int curX, curY;
	private int resX, resY, minX, maxX, minY, maxY;

	private Paint paint;
	private DatabaseManager dbm;
	private MainView context;

	// private Trip currentTrip;
	// private List<TimedStop> currentTimedStops;
	// private List<Stop> currentStops;
	private ActiveTripController activeTripController;

	// private List<PathSegment> path = null;
	// private PathSegment closest;

	public TransitMap(Context context) {
		super(context);
		initializeView(context);
	}

	public TransitMap(Context context, AttributeSet attrs) {
		super(context, attrs);
		initializeView(context);
	}

	public TransitMap(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initializeView(context);
	}

	/**
	 * Sets the map up once started.
	 * 
	 * @param viewArea
	 *            The area to view on the screen in metres.
	 * @param lat
	 *            The start latitude
	 * @param lon
	 *            The start longitude
	 */
	public void initialize(int viewArea, double lat, double lon) {
		this.dbm = DatabaseManager.getInstance();
		scaleFactor = viewArea;
		setZoom((int) scaleFactor);
		this.curX = (int) (lon * Utilities.COORDMULT);
		this.curY = (int) (lat * Utilities.COORDMULT);
	}

	/**
	 * Sets the zoom on the map. The map will display this much on its smallest
	 * dimension.
	 * 
	 * @param metresToShow
	 */
	public void setZoom(int metresToShow) {
		// convert view area (in meters) into decimal degrees.
		viewArea = metresToShow * (Utilities.COORDMULT / 111000);
		context.updateAppStatus("Zoom is now " + metresToShow + " (" + viewArea
				+ ")");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.uvic.dmcilvan.transittrackerV2.control.ActiveTripControllerObserver
	 * #notify(com.uvic.dmcilvan.transittrackerV2.control.ActiveTripController)
	 */
	public void notify(ActiveTripController activeTripController) {
		this.activeTripController = activeTripController;
		this.postInvalidate();
	}

	/**
	 * Does private initialization before initialize() is called.
	 * 
	 * @param context
	 *            The Context which created this map.
	 */
	private void initializeView(Context context) {
		this.context = (MainView) context;
		// activeTripController = ActiveTripController.getInstance();
		paint = new Paint();
		detector = new ScaleGestureDetector(getContext(), new ScaleListener());
	}

	/**
	 * Starts the map drawing information.
	 * 
	 * @param active
	 */
	public void activateMap(boolean active) {
		if (active != this.active) {
			this.postInvalidate();
		}
		this.active = active;
	}

	/*
	 * (non-Javadoc) Draws the map.
	 * 
	 * @see android.view.View#onDraw(android.graphics.Canvas)
	 */
	@Override
	public void onDraw(Canvas canvas) {

		if (!active || activeTripController == null) {
			// Paint background
			paint.setColor(Color.BLACK);
			paint.setStyle(Style.FILL);
			canvas.drawRect(0, 0, this.getWidth(), this.getHeight(), paint);

			// Paint a waiting message.
			paint.setColor(Color.RED);
			paint.setStyle(Style.FILL_AND_STROKE);
			paint.setTextSize(50);
			canvas.drawText("Waiting on Database", 10, this.getHeight() / 2,
					paint);
			paint.setTextSize(30);
			canvas.drawText("Connect to WIFI and restart app to download", 10,
					this.getHeight() / 2 + 40, paint);
			canvas.drawText("complete database "
					+ "or wait to generate (takes hours)", 10,
					this.getHeight() / 2 + 80, paint);

		} else {
			// lock the activeTripController so no changes are made while
			// iterating through lists.
			activeTripController.lock();

			this.curX = activeTripController.getLon();
			this.curY = activeTripController.getLat();

			// Find screen resolution
			resX = this.getWidth();
			resY = this.getHeight();
			// System.out.println(resX + ", " + resY);

			// calculate how many decimal degrees per pixel
			if (resX < resY) {
				// System.out.println("DrawMultX:" + resX + "/" + viewArea);
				drawMult = viewArea / resX;
				minX = curX - viewArea / 2;
				maxX = curX + viewArea / 2;
				minY = (int) (curY - (viewArea / 2) * ((float) resY / resX));
				maxY = (int) (curY + (viewArea / 2) * ((float) resY / resX));
			} else {
				// System.out.println("DrawMultY:" + resY + "/" + viewArea);
				drawMult = viewArea / resY;
				minY = curY - viewArea / 2;
				maxY = curY + viewArea / 2;
				minX = (int) (curX - (viewArea / 2) * ((float) resX / resY));
				maxX = (int) (curX + (viewArea / 2) * ((float) resX / resY));
			}
			width = maxX - minX;
			height = maxY - minY;
			if (drawMult < 1) {
				drawMult = 1;
			}

			// Paint background
			paint.setColor(Color.BLACK);
			paint.setStyle(Style.FILL);
			canvas.drawRect(0, 0, resX, resY, paint);

			// Draw the areas the current route is listed for routeSearch.
			if (activeTripController.getRoute() != null) {
				paint.setColor(Color.CYAN);
				paint.setStyle(Style.FILL);
				paint.setAlpha(50);
				for (Point p : dbm.routeSearchGrids(activeTripController
						.getRoute().getId())) {
					canvas.drawRect(multX(p.x),
							multY(p.y + Utilities.ROUNDING), multX(p.x
									+ Utilities.ROUNDING), multY(p.y), paint);
				}
			}

			// Paint all stops currently visible
			paint.setColor(Color.RED);
			paint.setStyle(Style.STROKE);
			for (Stop s : dbm.getStopsByArea(minX, maxX, minY, maxY)) {
				canvas.drawCircle(multX(s.getX()), multY(s.getY()), 5, paint);
			}

			// Paint stops for the current trip
			if (activeTripController.getCurrentStops() != null) {
				paint.setColor(Color.YELLOW);
				paint.setStyle(Style.STROKE);
				for (Stop s : activeTripController.getCurrentStops()) {
					canvas.drawCircle(multX(s.getX()), multY(s.getY()), 5,
							paint);
				}
			}

			// Paint path segments for the current trip
			if (activeTripController.getPath() != null) {
				paint.setColor(Color.MAGENTA);
				paint.setStyle(Style.STROKE);
				for (PathSegment p : activeTripController.getPath()) {
					canvas.drawRect(multX(p.lon), multY(p.lat
							+ Utilities.ROUNDING), multX(p.lon
							+ Utilities.ROUNDING), multY(p.lat), paint);
				}
			}

			// Paint all active buses in the system
			for (Bus b : activeTripController.getActiveBuses()) {
				paint.setColor(Color.WHITE);
				paint.setStyle(Style.FILL_AND_STROKE);
				if (!(b.currentLocation == null || b.lastLocation == null)) {
					int lat = b.currentLocation.lat + Utilities.ROUNDING / 2;
					int lon = b.currentLocation.lon + Utilities.ROUNDING / 2;
					int lastLat = b.lastLocation.lat + Utilities.ROUNDING / 2;
					int lastLon = b.lastLocation.lon + Utilities.ROUNDING / 2;
					canvas.drawCircle(multX(lon), multY(lat), 2, paint);
					canvas.drawLine(multX(lon), multY(lat), multX(lastLon),
							multY(lastLat), paint);
				}
			}

			// Paint calculated location of the bus on the current trip
			if (activeTripController.getCalculatedLocation() != null) {
				paint.setColor(Color.YELLOW);
				paint.setStyle(Style.STROKE);
				PathSegment p = activeTripController.getCalculatedLocation();
				canvas.drawRect(multX(p.lon),
						multY(p.lat + Utilities.ROUNDING), multX(p.lon
								+ Utilities.ROUNDING), multY(p.lat), paint);
				canvas.drawLine(multX(curX), multY(curY), multX(p.lon
						+ Utilities.ROUNDING / 2), multY(p.lat
						+ Utilities.ROUNDING / 2), paint);
			}

			// Paint current location along the trip
			paint.setColor(Color.CYAN);
			paint.setStyle(Style.FILL);
			canvas.drawCircle(multX(curX), multY(curY), 5, paint);

			if (activeTripController.getClosest() != null) {
				canvas.drawLine(multX(curX), multY(curY),
						multX(activeTripController.getClosest().lon
								+ Utilities.ROUNDING / 2),
						multY(activeTripController.getClosest().lat
								+ Utilities.ROUNDING / 2), paint);
			}

			// Mark stop the bus should be closest to
			if (activeTripController.getTimedStops() != null) {
				paint.setColor(Color.GREEN);
				paint.setStyle(Style.FILL);
				int best = Integer.MAX_VALUE;
				TimedStop closest = null;
				for (TimedStop s : activeTripController.getTimedStops()) {
					if (Math.abs(s.getArrivalTime()
							- Utilities.getCurrentTime()) < best) {
						best = Math.abs(s.getArrivalTime()
								- Utilities.getCurrentTime());
						closest = s;
					}
				}
				Stop stop = dbm.getStopById(closest.getStopId());
				if (stop != null)
					canvas.drawCircle(multX(stop.getX()), multY(stop.getY()),
							5, paint);
			}

			// Draws the path the device has taken
			if (activeTripController.getHistoricPath() != null) {
				paint.setColor(Color.CYAN);
				paint.setStyle(Style.FILL_AND_STROKE);
				for (PathSegment p : activeTripController.getHistoricPath()) {
					canvas.drawCircle(multX(p.lon), multY(p.lat), 10, paint);
				}
			}

			// draws the trip in time view
			if (activeTripController.getPath() != null) {
				paint.setStyle(Style.FILL_AND_STROKE);
				int currentTime = Utilities.getCurrentTime();
				Route r = activeTripController.getRoute();
				int routeTime = r.getMaxTravelTime();
				for (Trip t : DatabaseManager.getInstance().getTripsByRoute(
						activeTripController.getRoute().getId())) {
					if (t.getStartTime() + routeTime > currentTime
							&& t.getEndTime() - routeTime < currentTime) {
						PathSegment last = null;
						if (t.getId().equals(
								activeTripController.getTrip().getId())) {
							paint.setColor(Color.YELLOW);
							paint.setAlpha(160);
						} else {
							paint.setColor(Color.CYAN);
							paint.setAlpha(90);
						}
						for (PathSegment p : t.getPath()) {
							if (last == null) {
								last = p;
							}
							canvas.drawLine(multX(p.lon),
									multY((int) (Utilities
											.timeToDist(p.arrivalTime
													- currentTime) + curY)),
									multX(last.lon), multY((int) (Utilities
											.timeToDist(last.arrivalTime
													- currentTime) + curY)),
									paint);
							canvas.drawCircle(multX(p.lon),
									multY((int) (Utilities
											.timeToDist(p.arrivalTime
													- currentTime) + curY)), 3,
									paint);
							last = p;
						}
					}
				}
			}

			// draws the historic path in time view
			List<PathSegment> historicPath = activeTripController
					.getHistoricPath();
			if (historicPath != null) {
				paint.setColor(Color.GRAY);
				paint.setStyle(Style.FILL_AND_STROKE);
				paint.setAlpha(120);
				int currentTime = Utilities.getCurrentTime();
				PathSegment last = null;
				for (PathSegment p : historicPath) {
					if (p == null) {
						System.out.println("p is null");
						break;
					}
					if (last == null) {
						last = p;
					}
					canvas.drawLine(
							multX(p.lon),
							multY((int) (Utilities.timeToDist(p.arrivalTime
									- currentTime) + curY)),
							multX(last.lon),
							multY((int) (Utilities.timeToDist(last.arrivalTime
									- currentTime) + curY)), paint);
					last = p;
				}

			}
			activeTripController.unlock();
		}

	}

	private int multX(int decimalDegX) {
		// System.out.println("Printing at X:" + ((decimalDegX - curX - viewArea
		// / 2) * drawMult));
		return (decimalDegX - curX + width / 2) / drawMult;
	}

	private int multY(int decimalDegY) {
		// System.out.println("Printing at Y:" + ((curY + viewArea / 2 -
		// decimalDegY) * drawMult));
		return (height / 2 + (curY - decimalDegY)) / drawMult;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		detector.onTouchEvent(event);
		return true;
	}

	private class ScaleListener extends
			ScaleGestureDetector.SimpleOnScaleGestureListener {
		@Override
		public boolean onScale(ScaleGestureDetector detector) {
			scaleFactor *= detector.getScaleFactor();
			scaleFactor = Math.max(MIN_ZOOM, Math.min(scaleFactor, MAX_ZOOM));
			setZoom((int) scaleFactor);
			postInvalidate();
			return true;
		}
	}
}
