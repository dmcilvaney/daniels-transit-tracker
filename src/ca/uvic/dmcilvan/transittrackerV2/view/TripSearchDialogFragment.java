package ca.uvic.dmcilvan.transittrackerV2.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.Button;

import ca.uvic.dmcilvan.transittrackerV2.R;
import ca.uvic.dmcilvan.transittrackerV2.control.ActiveTripController;
import ca.uvic.dmcilvan.transittrackerV2.entity.Trip;


public class TripSearchDialogFragment extends DialogFragment {

	Button btn;
	static String dialogTitle = "Select a Trip";

	public interface TripSeachDialogListener {
		void onFinishInputDialog(Trip selectedTrip);
	}

	static TripSearchDialogFragment newInstance() {
		TripSearchDialogFragment dialog = new TripSearchDialogFragment();
		return dialog;
	}

	public TripSearchDialogFragment() {

	}

	public Dialog onCreateDialog(Bundle savedInstanceState) {
		System.out.println("Start onCreate");
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(R.string.pick_trip);
		builder.setItems(
				ActiveTripController.getInstance().getTripSearchList(),
				new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						ActiveTripController.getInstance().onFinishInputDialog(
								which);
					}
				});
		System.out.println("End onCreate2");
		return builder.create();
	}

	// @Override
	// public View

	//
	// Dialog dialog;
	// ListView tripList;
	// String[] val = { "sunday", "monday", "tuesday", "thrusday", "friday",
	// "wednesday", "march" };
	//
	// public void onCreate(Bundle savedInstanceState) {
	// super.onCreate(savedInstanceState);
	// showDialog();
	// }
	//
	// private void showDialog() {
	// dialog = new Dialog(this);
	// dialog.setTitle("Select a trip");
	// LayoutInflater inflater = (LayoutInflater) this
	// .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	// View view = inflater
	// .inflate(R.layout.trip_search_dialogue, null, false);
	// dialog.setContentView(view);
	// dialog.setCancelable(false);
	//
	// tripList = (ListView) dialog.findViewById(R.id.triplist);
	// tripList.setOnItemClickListener(this);
	// tripList.setAdapter(new ArrayAdapter<String>(this,
	// android.R.layout.simple_list_item_1, val));
	// dialog.show();
	// }
	//
	// public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long
	// arg3) {
	// // TODO Auto-generated method stub
	//
	// }

}
